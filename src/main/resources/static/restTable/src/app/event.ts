export class Event {
    name:string;
    dateStart: number;
    dateEnd: number;
    agile:boolean;
    waterfall:boolean;

}
