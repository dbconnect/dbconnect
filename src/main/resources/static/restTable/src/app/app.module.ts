import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HttpModule} from '@angular/http';
import {EventService} from './service/event.service';
import {BsDatepickerModule} from 'ngx-bootstrap';
import {BsDropdownModule} from 'ngx-bootstrap/dropdown';
import {AppComponent} from './app.component';
import {ListeventsComponent} from './components/listevents/listevents.component';
import {StatisticsComponent} from './components/statistics/statistics.component';
import {HomeComponent} from './components/home/home.component';
import {FusionChartsModule} from "angular4-fusioncharts";
import {StatisticsService} from "./service/statistics.service";
import {NgxPopupModule} from 'ngx-popups';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations'
import {MatButtonModule, MatCheckboxModule} from '@angular/material';
import {BootstrapAlertModule} from 'ngx-bootstrap-alert-service';
import {Ng4LoadingSpinnerModule} from 'ng4-loading-spinner';

import * as FusionCharts from 'fusioncharts';
import * as Charts from 'fusioncharts/fusioncharts.charts';
import * as FintTheme from 'fusioncharts/themes/fusioncharts.theme.fint';
import {AlertModule} from 'ngx-alerts';
import {TutorialComponent} from './components/tutorial/tutorial.component';
import {ContactComponent} from './components/contact/contact.component';

const appRoutes: Routes=[
  {path:'', redirectTo:'/home', pathMatch:'full'},
  {path:'home',component:HomeComponent},
  {path:'events',component:ListeventsComponent},
  {path:'statistics', component:StatisticsComponent},
  {path: 'tutorial', component: TutorialComponent},
  {path: 'contact', component: ContactComponent}
]

FusionChartsModule.fcRoot(FusionCharts, Charts, FintTheme);

@NgModule({
  declarations: [
    AppComponent,
    ListeventsComponent,
    StatisticsComponent,
    HomeComponent,
    TutorialComponent,
    ContactComponent
  ],
  imports: [
    BsDatepickerModule.forRoot(),
    BrowserModule,
    HttpModule,
    BsDropdownModule.forRoot(),
    RouterModule.forRoot(appRoutes),
    FusionChartsModule,
    NgxPopupModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatCheckboxModule,
    BootstrapAlertModule,
    AlertModule.forRoot({maxMessages: 1, timeout: 3000}),
    Ng4LoadingSpinnerModule.forRoot()
  ],
  providers: [EventService, StatisticsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
