export class OrarEvent {
    hour: number;
    agile:Boolean;
    agileEventId: any;
    waterfall:Boolean;
    wateEventId:any;
    agileEventName: string;
    waterfallEventName: string;
    deleteAgile: Boolean;
    deleteWaterfall: Boolean;

}
