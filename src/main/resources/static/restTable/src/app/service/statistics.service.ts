import {Injectable} from '@angular/core';
import {Headers, Http, RequestOptions, Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable()
export class StatisticsService {

  private baseUrl: string = 'http://localhost:8080/api1';
  private headers = new Headers({'Content-Type':'application/json'});
  private options = new RequestOptions({headers:this.headers});

  constructor(private _http:Http) {   }

  getTheMostPopularHourByMonthAgileAndWaterfall() {
    return this._http.get(this.baseUrl + '/statistics/12', this.options).map((response: Response) => response.json())
      .catch(this.errorHandler);
  }

  getTheMostPopularHourByMonthWaterfall() {
    return this._http.get(this.baseUrl + '/statistics/11', this.options).map((response: Response) => response.json())
      .catch(this.errorHandler);
  }

  getTheMostPopularHourByMonthAgile() {
    return this._http.get(this.baseUrl + '/statistics', this.options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }

  getTheTotalHoursOfAMonthAgile() {
    return this._http.get(this.baseUrl + '/statistics/2', this.options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }

  getTheTotalHoursOfAMonthWaterfall() {
    return this._http.get(this.baseUrl + '/statistics/22', this.options).map((response: Response) => response.json())
      .catch(this.errorHandler);
  }

  getTheTotalHoursOfAMonthAgileAndWaterfall() {
    return this._http.get(this.baseUrl + '/statistics/23', this.options).map((response: Response) => response.json())
      .catch(this.errorHandler);
  }

  getLeastBusyHoursByDaysAgile() {
    return this._http.get(this.baseUrl + '/statistics/31', this.options).map((response: Response) => response.json())
      .catch(this.errorHandler);
  }

  getLeastBusyHoursByDaysWaterfall() {
    return this._http.get(this.baseUrl + '/statistics/32', this.options).map((response: Response) => response.json())
      .catch(this.errorHandler);
  }

  getLeastBusyHoursByDaysAgileAndWaterfall() {
    return this._http.get(this.baseUrl + '/statistics/33', this.options).map((response: Response) => response.json())
      .catch(this.errorHandler);
  }

  getMostEventsPerPeriodOfDayAgile() {
    return this._http.get(this.baseUrl + '/statistics/34', this.options).map((response: Response) => response.json())
      .catch(this.errorHandler);
  }

  getMostEventsPerPeriodOfDayWaterfall() {
    return this._http.get(this.baseUrl + '/statistics/35', this.options).map((response: Response) => response.json())
      .catch(this.errorHandler);
  }

  getMostEventsPerPeriodOfDayAgileAndWaterfall() {
    return this._http.get(this.baseUrl + '/statistics/36', this.options).map((response: Response) => response.json())
      .catch(this.errorHandler);
  }

  getMostPopularHoursByDaysOfWeekAgile(){
    return this._http.get(this.baseUrl + '/statistics/27', this.options).map((response: Response) => response.json())
      .catch(this.errorHandler);
  }

  getMostPopularHoursByDaysOfWeekWaterfall(){
    return this._http.get(this.baseUrl + '/statistics/28', this.options).map((response: Response) => response.json())
      .catch(this.errorHandler);
  }

  getMostPopularHoursByDaysOfWeekAgileAndWaterfall(){
    return this._http.get(this.baseUrl + '/statistics/29', this.options).map((response: Response) => response.json())
    .catch(this.errorHandler);
  }

  leastOccupiedHoursByDayAgile() {
    return this._http.get(this.baseUrl + '/statistics/50', this.options).map((response: Response) => response.json())
      .catch(this.errorHandler);
  }

  leastOccupiedHoursByDayWaterfall() {
    return this._http.get(this.baseUrl + '/statistics/51', this.options).map((response: Response) => response.json())
      .catch(this.errorHandler);
  }

  leastOccupiedHoursByDayAgileAndWaterfall() {
    return this._http.get(this.baseUrl + '/statistics/52', this.options).map((response: Response) => response.json())
      .catch(this.errorHandler);
  }

  errorHandler(error:Response){
    return Observable.throw(error ||'Server error');
  }

}
