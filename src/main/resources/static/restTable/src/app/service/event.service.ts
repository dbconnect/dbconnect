import {Injectable} from '@angular/core';
import {Headers, Http, RequestOptions, Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable()
export class EventService {
  private baseUrl: string = 'http://localhost:8080/api';
  private headers = new Headers({'Content-Type': 'application/json'});
  private options = new RequestOptions({headers: this.headers});

  constructor(private _http: Http) {
  }

  getOrarEvents() {
    return this._http.get(this.baseUrl + '/eventsList', this.options).map((response: Response) => response.json())
      .catch(this.errorHandler);

  }

  getOrarEventsByDate(dateCalendar: String) {
    console.log(dateCalendar);
    return this._http.get(this.baseUrl + '/eventsList/' + dateCalendar, this.options).map((response: Response) => response.json())
      .catch(this.errorHandler);

  }

  getOrarEventsByCode(code: String) {
    console.log(code);
    return this._http.get(this.baseUrl + '/eventsListValidate/' + code, this.options).map((response: Response) => response.json())
      .catch(this.errorHandler);

  }

  deleteEvent(id: Number) {
    return this._http.get(this.baseUrl + '/eventsList2/' + id, this.options).map((response: Response) => response.json())
      .catch(this.errorHandler);
  }

  errorHandler(error: Response) {
    return Observable.throw(error || 'Server error');
  }


}
