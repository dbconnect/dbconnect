import {Component, OnInit} from '@angular/core';
import {StatisticsService} from "../../service/statistics.service";
import {AlertService} from 'ngx-alerts';

@Component({
  moduleId: module.id,
  selector: 'app-statistics',
  templateUrl: './statistics.component.html',
  styleUrls: ['./statistics.component.css'],

})

export class StatisticsComponent implements OnInit {

  private statisticsInfo:any[];
  private dateStatistics:string;
  private display: boolean = false;

  constructor(private alertService: AlertService, private _statisticsService: StatisticsService) {
  }

  ngOnInit() {
  }

  msg(msg:string) {
    this.dateStatistics = msg;

    this.display = true;

    if (this.dateStatistics.valueOf() === "month-popular-both")
    {
      console.log(this.dateStatistics);
      this.type = 'column2d';
      this._statisticsService.getTheMostPopularHourByMonthAgileAndWaterfall().subscribe((statisticsInfo) => {
        console.log(statisticsInfo);
        this.statisticsInfo = statisticsInfo;
       this.dataSource = {
          "chart": {
            "caption": "Booking Statistics",
            "subCaption": "Most popular hours by month - Agile & Waterfall",
            "numbersufix": "h",
            "theme": "fint",
            "baseFont": "Verdana",
            "baseFontSize": "11",
            "baseFontColor": "#0066cc",
            "xAxisName": "Months",
            "yAxisName": "Hours",
            "animation": 1,
            "animationDuration": 1,
            "palettecolors": "#00487c,#4bb3fd,#3e6680,#0496ff,#027bce"
          },
         "data": this.statisticsInfo
       }
      }, (error) => {
        console.log(error);
      })
      this.statisticsInfo = null;
      this.dateStatistics = null;
    }
    else if (this.dateStatistics.valueOf() === "month-popular-agile") {
      console.log(this.dateStatistics);
      this.type = 'column2d';
      this._statisticsService.getTheMostPopularHourByMonthAgile().subscribe((statisticsInfo) => {
        console.log(statisticsInfo);
        this.statisticsInfo = statisticsInfo;
        this.dataSource = {
          "chart": {
            "caption": "Booking Statistics",
            "subCaption": "Most popular hours by month - Agile",
            "numbersufix": "h",
            "theme": "fint",
            "baseFont": "Verdana",
            "baseFontSize": "11",
            "baseFontColor": "#0066cc",
            "xAxisName": "Months",
            "yAxisName": "Hours",
            "animation": 1,
            "animationDuration": 1,
            "palettecolors": "#00487c,#4bb3fd,#3e6680,#0496ff,#027bce"
          },
          "data": this.statisticsInfo
        }
      }, (error) => {
        console.log(error);
      })
      this.statisticsInfo = null;
      this.dateStatistics = null;
    }
    else if (this.dateStatistics.valueOf() === "month-popular-waterfall") {
      console.log(this.dateStatistics);
      this.type = 'column2d';
      this._statisticsService.getTheMostPopularHourByMonthWaterfall().subscribe((statisticsInfo) => {
        console.log(statisticsInfo);
        this.statisticsInfo = statisticsInfo;
        this.dataSource = {
          "chart": {
            "caption": "Booking Statistics",
            "subCaption": "Most popular hours by month - Waterfall",
            "numbersufix": "h",
            "theme": "fint",
            "baseFont": "Verdana",
            "baseFontSize": "11",
            "baseFontColor": "#0066cc",
            "xAxisName": "Months",
            "yAxisName": "Hours",
            "animation": 1,
            "animationDuration": 1,
            "palettecolors": "#00487c,#4bb3fd,#3e6680,#0496ff,#027bce"
          },
          "data": this.statisticsInfo }
      }, (error) => {
        console.log(error);
      })
    }
    else if (this.dateStatistics.valueOf() === "month-nohours-both")
    {
      console.log(this.dateStatistics);
      this.type = 'column2d';
      this._statisticsService.getTheTotalHoursOfAMonthAgileAndWaterfall().subscribe((statisticsInfo) => {
        console.log(statisticsInfo);
        this.statisticsInfo = statisticsInfo;
        this.dataSource = {
          "chart": {
            "caption": "Booking Statistics",
            "subCaption": "The average of occupied hours by month - Agile & Waterfall",
            "numbersufix": "h",
            "theme": "fint",
            "baseFont": "Verdana",
            "baseFontSize": "11",
            "baseFontColor": "#0066cc",
            "xAxisName": "Months",
            "yAxisName": "Number of Hours",
            "animation": 1,
            "animationDuration": 1,
            "palettecolors": "#00487c,#4bb3fd,#3e6680,#0496ff,#027bce"
          },
          "data": this.statisticsInfo
        }
      }, (error) => {
        console.log(error);
      })
      this.statisticsInfo = null;
      this.dateStatistics = null;
    }
    else if (this.dateStatistics.valueOf() === "month-nohours-agile") {
      console.log(this.dateStatistics);
      this.type = 'column2d';
      this._statisticsService.getTheTotalHoursOfAMonthAgile().subscribe((statisticsInfo) => {
        console.log(statisticsInfo);
        this.statisticsInfo = statisticsInfo;
       this.dataSource = {
          "chart": {
            "caption": "Booking Statistics",
            "subCaption": "The average of occupied hours by month - Agile",
            "numbersufix": "h",
            "theme": "fint",
            "baseFont": "Verdana",
            "baseFontSize": "11",
            "baseFontColor": "#0066cc",
            "xAxisName": "Months",
            "yAxisName": "Number of Hours",
            "animation": 1,
            "animationDuration": 1,
            "palettecolors": "#00487c,#4bb3fd,#3e6680,#0496ff,#027bce"
          },
         "data": this.statisticsInfo
        }
      }, (error) => {
        console.log(error);
      })
      this.statisticsInfo = null;
      this.dateStatistics = null;
    }
    else if (this.dateStatistics.valueOf() === "month-nohours-waterfall") {
      console.log(this.dateStatistics);
      this.type = 'column2d';
      this._statisticsService.getTheTotalHoursOfAMonthWaterfall().subscribe((statisticsInfo) => {
        console.log(statisticsInfo);
        this.statisticsInfo = statisticsInfo;
        this.dataSource = {
          "chart": {
            "caption": "Booking Statistics",
            "subCaption": "The average of occupied hours by month - Waterfall",
            "numbersufix": "h",
            "theme": "fint",
            "baseFont": "Verdana",
            "baseFontSize": "11",
            "baseFontColor": "#0066cc",
            "xAxisName": "Months",
            "yAxisName": "Number of Hours",
            "animation": 1,
            "animationDuration": 1,
            "palettecolors": "#00487c,#4bb3fd,#3e6680,#0496ff,#027bce"
          },
          "data": this.statisticsInfo
        }
      }, (error) => {
        console.log(error);
      })
      this.statisticsInfo = null;
      this.dateStatistics = null;
    }
    else if (this.dateStatistics.valueOf() === "day-nohours-agile") {
      console.log(this.dateStatistics);
      this.type = 'column2d';
      this._statisticsService.getLeastBusyHoursByDaysAgile().subscribe((statisticsInfo) => {
        console.log(statisticsInfo);
        this.statisticsInfo = statisticsInfo;
        this.dataSource = {
          "chart": {
            "caption": "Booking Statistics",
            "subCaption": "The average of hours occupied by days of week - Agile",
            "numbersufix": "h",
            "theme": "fint",
            "baseFont": "Verdana",
            "baseFontSize": "11",
            "baseFontColor": "#0066cc",
            "xAxisName": "Days of week",
            "yAxisName": "Hours",
            "animation": 1,
            "animationDuration": 1,
            "palettecolors": "#00487c,#4bb3fd,#3e6680,#0496ff,#027bce"
          },
          "data": this.statisticsInfo,
          "trendlines": [
            {
              "line": [
                {
                  "startvalue": "6",
                  "color": "#1aaf5d",
                  "valueOnRight": "1",
                  "displayvalue": "Monthly Target"
                }
              ]
            }
          ],
        }
      }, (error) => {
        console.log(error);
      })
      this.statisticsInfo = null;
      this.dateStatistics = null;
    }
    else if (this.dateStatistics.valueOf() === "day-nohours-waterfall") {
      console.log(this.dateStatistics);
      this.type = 'column2d';
      this._statisticsService.getLeastBusyHoursByDaysWaterfall().subscribe((statisticsInfo) => {
        console.log(statisticsInfo);
        this.statisticsInfo = statisticsInfo;
        this.dataSource = {
          "chart": {
            "caption": "Booking Statistics",
            "subCaption": "The average of hours occupied by days of week - Waterfall",
            "numbersufix": "h",
            "theme": "fint",
            "baseFont": "Verdana",
            "baseFontSize": "11",
            "baseFontColor": "#0066cc",
            "paletteColors": "#0075c2, #0066cc",
            "xAxisName": "Days of week",
            "yAxisName": "Hours",
            "animation": 1,
            "animationDuration": 1,
            "palettecolors": "#00487c,#4bb3fd,#3e6680,#0496ff,#027bce"
          },
          "data": this.statisticsInfo,
          "trendlines": [
            {
              "line": [
                {
                  "startvalue": "6",
                  "color": "#1aaf5d",
                  "valueOnRight": "1",
                  "displayvalue": "Monthly Target"
                }
              ]
            }
          ],
        }
      }, (error) => {
        console.log(error);
      })
      this.statisticsInfo = null;
      this.dateStatistics = null;
    }
    else if (this.dateStatistics.valueOf() === "day-nohours-both") {
      console.log(this.dateStatistics);
      this.type = 'column2d';
      this._statisticsService.getLeastBusyHoursByDaysAgileAndWaterfall().subscribe((statisticsInfo) => {
        console.log(statisticsInfo);
        this.statisticsInfo = statisticsInfo;
        this.dataSource = {
          "chart": {
            "caption": "Booking Statistics",
            "subCaption": "The average of hours occupied by days of week - Agile & Waterfall",
            "numbersufix": "h",
            "theme": "fint",
            "baseFont": "Verdana",
            "baseFontSize": "11",
            "baseFontColor": "#0066cc",
            "xAxisName": "Days of week",
            "yAxisName": "Hours",
            "animation": 1,
            "animationDuration": 1,
            "palettecolors": "#00487c,#4bb3fd,#3e6680,#0496ff,#027bce"
          },
          "data": this.statisticsInfo,
          "trendlines": [
            {
              "line": [
                {
                  "startvalue": "6",
                  "color": "#1aaf5d",
                  "valueOnRight": "1",
                  "displayvalue": "Monthly Target"
                }
              ]
            }
          ],
        }
      }, (error) => {
        console.log(error);
      })
      this.statisticsInfo = null;
      this.dateStatistics = null;
    }
    else if (this.dateStatistics.valueOf() === "part-noevents-agile") {
      console.log(this.dateStatistics);
      this.type = "pie2d"
      this._statisticsService.getMostEventsPerPeriodOfDayAgile().subscribe((statisticsInfo) => {
        console.log(statisticsInfo);
        this.statisticsInfo = statisticsInfo;
        this.dataSource = {
          "chart": {
            "caption": "Booking Statistics",
            "subCaption": "Total number of events by period of day - Agile",
            "numbersufix": "h",
            "theme": "fint",
            "baseFont": "Verdana",
            "baseFontSize": "11",
            "baseFontColor": "#0066cc",
            "palettecolors": "#00487c,#4bb3fd,#3e6680,#0496ff,#027bce",
            "bgColor": "#ffffff",
            "showBorder": "0",
            "use3DLighting": "0",
            "showShadow": "0",
            "enableSmartLabels": "0",
            "startingAngle": "0",
            "showPercentValues": "1",
            "showPercentInTooltip": "0",
            "decimals": "1",
            "captionFontSize": "14",
            "subcaptionFontSize": "14",
            "subcaptionFontBold": "0",
            "toolTipColor": "#ffffff",
            "toolTipBorderThickness": "0",
            "toolTipBgColor": "#000000",
            "toolTipBgAlpha": "80",
            "toolTipBorderRadius": "2",
            "toolTipPadding": "5",
            "showHoverEffect": "1",
            "showLegend": "1",
            "legendBgColor": "#ffffff",
            "legendBorderAlpha": "0",
            "legendShadow": "0",
            "legendItemFontSize": "10",
            "legendItemFontColor": "#666666",
            "useDataPlotColorForLabels": "1",
            "animation": 1,
            "animationDuration": 1
          },
          "data": this.statisticsInfo
        }
      }, (error) => {
        console.log(error);
      })
      this.statisticsInfo = null;
      this.dateStatistics = null;
    }
    else if (this.dateStatistics.valueOf() === "part-noevents-waterfall") {
      console.log(this.dateStatistics);
      this.type = "pie2d"
      this._statisticsService.getMostEventsPerPeriodOfDayWaterfall().subscribe((statisticsInfo) => {
        console.log(statisticsInfo);
        this.statisticsInfo = statisticsInfo;
        this.dataSource = {
          "chart": {
            "caption": "Booking Statistics",
            "subCaption": "Total number of events by period of day - Waterfall",
            "numbersufix": "h",
            "theme": "fint",
            "baseFont": "Verdana",
            "baseFontSize": "11",
            "baseFontColor": "#0066cc",
            "palettecolors": "#00487c,#4bb3fd,#3e6680,#0496ff,#027bce",
            "bgColor": "#ffffff",
            "showBorder": "0",
            "use3DLighting": "0",
            "showShadow": "0",
            "enableSmartLabels": "0",
            "startingAngle": "0",
            "showPercentValues": "1",
            "showPercentInTooltip": "0",
            "decimals": "1",
            "captionFontSize": "14",
            "subcaptionFontSize": "14",
            "subcaptionFontBold": "0",
            "toolTipColor": "#ffffff",
            "toolTipBorderThickness": "0",
            "toolTipBgColor": "#000000",
            "toolTipBgAlpha": "80",
            "toolTipBorderRadius": "2",
            "toolTipPadding": "5",
            "showHoverEffect": "1",
            "showLegend": "1",
            "legendBgColor": "#ffffff",
            "legendBorderAlpha": "0",
            "legendShadow": "0",
            "legendItemFontSize": "10",
            "legendItemFontColor": "#666666",
            "useDataPlotColorForLabels": "1",
            "animation": 1,
            "animationDuration": 1
          },
          "data": this.statisticsInfo
        }
      }, (error) => {
        console.log(error);
      })
      this.statisticsInfo = null;
      this.dateStatistics = null;
    }
    else if (this.dateStatistics.valueOf() === "part-noevents-both") {
      console.log(this.dateStatistics);
      this.type = "pie2d"
      this._statisticsService.getMostEventsPerPeriodOfDayAgileAndWaterfall().subscribe((statisticsInfo) => {
        console.log(statisticsInfo);
        this.statisticsInfo = statisticsInfo;
        this.dataSource = {
          "chart": {
            "caption": "Booking Statistics",
            "subCaption": "Total number of events by period of day - Agile & Waterfall",
            "numbersufix": "h",
            "theme": "fint",
            "baseFont": "Verdana",
            "baseFontSize": "11",
            "baseFontColor": "#0066cc",
            "palettecolors": "#00487c,#4bb3fd,#3e6680,#0496ff,#027bce",
            "bgColor": "#ffffff",
            "showBorder": "0",
            "use3DLighting": "0",
            "showShadow": "0",
            "enableSmartLabels": "0",
            "startingAngle": "0",
            "showPercentValues": "1",
            "showPercentInTooltip": "0",
            "decimals": "1",
            "captionFontSize": "14",
            "subcaptionFontSize": "14",
            "subcaptionFontBold": "0",
            "toolTipColor": "#ffffff",
            "toolTipBorderThickness": "0",
            "toolTipBgColor": "#000000",
            "toolTipBgAlpha": "80",
            "toolTipBorderRadius": "2",
            "toolTipPadding": "5",
            "showHoverEffect": "1",
            "showLegend": "1",
            "legendBgColor": "#ffffff",
            "legendBorderAlpha": "0",
            "legendShadow": "0",
            "legendItemFontSize": "10",
            "legendItemFontColor": "#666666",
            "useDataPlotColorForLabels": "1",
            "animation": 1,
            "animationDuration": 1
          },
          "data": this.statisticsInfo,
          "palettecolors":"FF5904,0372AB",
          "plotHoverEffect":"1",
          "plotFillHoverColor":"#66b3ff",
        }
      }, (error) => {
        console.log(error);
      })
      this.statisticsInfo = null;
      this.dateStatistics = null;
    }
    else if (this.dateStatistics.valueOf() === "day-popular-both") {
      console.log(this.dateStatistics);
      this.type="bar2d"
      this._statisticsService.getMostPopularHoursByDaysOfWeekAgileAndWaterfall().subscribe((statisticsInfo) => {
        console.log(statisticsInfo);
        this.statisticsInfo = statisticsInfo;
        this.dataSource = {
          "chart": {
            "caption": "Booking Statistics",
            "subCaption": "Most popular hours by days of week - Agile & Waterfall",
            "numbersufix": "h",
            "theme": "fint",
            "baseFont": "Verdana",
            "baseFontSize": "11",
            "baseFontColor": "#0066cc",
            "xAxisName": "Days of week",
            "yAxisName": "Hours",
            "animation": 1,
            "animationDuration": 1,
            "palettecolors": "#00487c,#4bb3fd,#3e6680,#0496ff,#027bce"
          },
          "data": this.statisticsInfo
        }
      }, (error) => {
        console.log(error);
      })
      this.statisticsInfo = null;
      this.dateStatistics = null;
    }
    else if (this.dateStatistics.valueOf() === "day-popular-waterfall") {
      console.log(this.dateStatistics);
      this.type="bar2d"
      this._statisticsService.getMostPopularHoursByDaysOfWeekWaterfall().subscribe((statisticsInfo) => {
        console.log(statisticsInfo);
        this.statisticsInfo = statisticsInfo;
        this.dataSource = {
          "chart": {
            "caption": "Booking Statistics",
            "subCaption": "Most popular hours by days of week - Waterfall",
            "numbersufix": "h",
            "theme": "fint",
            "baseFont": "Verdana",
            "baseFontSize": "11",
            "baseFontColor": "#0066cc",
            "xAxisName": "Days of week",
            "yAxisName": "Hours",
            "animation": 1,
            "animationDuration": 1,
            "palettecolors": "#00487c,#4bb3fd,#3e6680,#0496ff,#027bce"
          },
          "data": this.statisticsInfo
        }
      }, (error) => {
        console.log(error);
      })
      this.statisticsInfo = null;
      this.dateStatistics = null;
    }
    else if (this.dateStatistics.valueOf() === "day-popular-agile") {
      console.log(this.dateStatistics);
      this.type="bar2d"
      this._statisticsService.getMostPopularHoursByDaysOfWeekAgile().subscribe((statisticsInfo) => {
        console.log(statisticsInfo);
        this.statisticsInfo = statisticsInfo;
        this.dataSource = {
          "chart": {
            "caption": "Booking Statistics",
            "subCaption": "Most popular hours by days of week - Agile",
            "numbersufix": "h",
            "theme": "fint",
            "baseFont": "Verdana",
            "baseFontSize": "11",
            "baseFontColor": "#0066cc",
            "xAxisName": "Days of week",
            "yAxisName": "Hours",
            "animation": 1,
            "animationDuration": 1,
            "palettecolors": "#00487c,#4bb3fd,#3e6680,#0496ff,#027bce"
          },
          "data": this.statisticsInfo
        }
      }, (error) => {
        console.log(error);
      })
      this.statisticsInfo = null;
      this.dateStatistics = null;
    }
    else if (this.dateStatistics.valueOf() === "day-least-agile") {
      console.log(this.dateStatistics);
      this.type = "line"
      this._statisticsService.leastOccupiedHoursByDayAgile().subscribe((statisticsInfo) => {
        console.log(statisticsInfo);
        this.statisticsInfo = statisticsInfo;
        this.dataSource = {
          "chart": {
            "caption": "Booking Statistics",
            "subCaption": "The least ocuppied hours by days of week - Agile",
            "theme": "fint",
            "baseFont": "Verdana",
            "numbersufix": "h",
            "baseFontSize": "11",
            "baseFontColor": "#0066cc",
            "xAxisName": "Days of week",
            "yAxisName": "Hour",
            "animation": 1,
            "animationDuration": 1,
            "palettecolors": "#00487c,#4bb3fd,#3e6680,#0496ff,#027bce"
          },
          "data": this.statisticsInfo
        }
      }, (error) => {
        console.log(error);
      })
      this.statisticsInfo = null;
      this.dateStatistics = null;
    }
    else if (this.dateStatistics.valueOf() === "day-least-waterfall") {
      console.log(this.dateStatistics);
      this.type = "line"
      this._statisticsService.leastOccupiedHoursByDayWaterfall().subscribe((statisticsInfo) => {
        console.log(statisticsInfo);
        this.statisticsInfo = statisticsInfo;
        this.dataSource = {
          "chart": {
            "caption": "Booking Statistics",
            "subCaption": "The least ocuppied hours by days of week - Waterfall",
            "numbersufix": "h",
            "theme": "fint",
            "baseFont": "Verdana",
            "baseFontSize": "11",
            "baseFontColor": "#0066cc",
            "xAxisName": "Days of week",
            "yAxisName": "Hour",
            "animation": 1,
            "animationDuration": 1,
            "palettecolors": "#00487c,#4bb3fd,#3e6680,#0496ff,#027bce"
          },
          "data": this.statisticsInfo
        }
      }, (error) => {
        console.log(error);
      })
      this.statisticsInfo = null;
      this.dateStatistics = null;
    }
    else if (this.dateStatistics.valueOf() === "day-least-both") {
      console.log(this.dateStatistics);
      this.type = "line"
      this._statisticsService.leastOccupiedHoursByDayAgileAndWaterfall().subscribe((statisticsInfo) => {
        console.log(statisticsInfo);
        this.statisticsInfo = statisticsInfo;
        this.dataSource = {
          "chart": {
            "caption": "Booking Statistics",
            "subCaption": "The least ocuppied hours by days of week - Agile & Waterfall",
            "theme": "fint",
            "baseFont": "Verdana",
            "baseFontSize": "11",
            "baseFontColor": "#0066cc",
            "xAxisName": "Days of week",
            "yAxisName": "Hour",
            "animation": 1,
            "animationDuration": 1,
            "palettecolors": "#00487c,#4bb3fd,#3e6680,#0496ff,#027bce"
          },
          "data": this.statisticsInfo
        }
      }, (error) => {
        console.log(error);
      })
      this.statisticsInfo = null;
      this.dateStatistics = null;
    }
    else {
      console.log("altceva");
      this.alertService.danger("This combination is not valid! Try another one!");
      this.statisticsInfo = null;
      this.dateStatistics = null;
    }
  }

  id = 'chart1';
  width = 700;
  height = 500;
  type = 'column2d';
  dataFormat = 'json';
  dataSource;
  title = 'Angular4 FusionCharts';

}
