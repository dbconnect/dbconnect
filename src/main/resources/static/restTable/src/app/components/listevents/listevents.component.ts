import {Component, OnInit} from '@angular/core';
import {EventService} from '../../service/event.service';
import {Event} from '../../event';
import {OrarEvent} from '../../orarEvent';
import {BsDatepickerConfig} from 'ngx-bootstrap';
import {AlertService} from "ngx-alerts";

@Component({
  selector: 'app-listevents',
  templateUrl: './listevents.component.html',
  styleUrls: ['./listevents.component.css']
})
export class ListeventsComponent implements OnInit {

  bsValue = new Date();
  bsRangeValue: Date[];
  maxDate = new Date();
  popupMessage: string;
  colorTheme = 'theme-dark-blue';
  bsConfig: Partial<BsDatepickerConfig>;
  buttonIsClicked: boolean;
  private events: Event[];
  private orarEvents: OrarEvent[];

  constructor(private _eventService: EventService, private alertSerice: AlertService) {
    this.maxDate.setDate(this.maxDate.getDate() + 7);
    this.bsRangeValue = [this.bsValue, this.maxDate];
    this.bsConfig = Object.assign({}, { containerClass: this.colorTheme ,dateInputFormat: 'YYYY-MM-DD', });


  }

  ngOnInit() {
    this._eventService.getOrarEvents().subscribe((orarEvents) => {
      console.log("evenimente sin ziua curenta");
      console.log(orarEvents);
      this.orarEvents= orarEvents;
    }, (error) => {
      console.log(error);
    });
    // document.getElementById("delete").style.display = "visible";
  }

  getOrarEventsByDate(dateCalendar:string){
    this._eventService.getOrarEventsByDate(dateCalendar).subscribe((orarEvents) => {
      console.log("evenimente dupa o data specificate");
      console.log(orarEvents);
      this.orarEvents= orarEvents;
      if (this.orarEvents.length == 1) {
        this.popupMessage = this.orarEvents[0].agileEventName;
        this.alertSerice.danger("You can NOT make a reservation on this date because it's " + this.popupMessage);
      }
      if (this.orarEvents.length == 0)
        this.alertSerice.danger("You can NOT make a reservation in weekend");
    }, (error) => {
      console.log(error);
    })
  }

  deleteEvent() {
    var id;
    this.buttonIsClicked = true;
    for (let orarEvent of this.orarEvents) {
      if (orarEvent.deleteWaterfall && orarEvent.deleteAgile) {
        id = orarEvent.agileEventId;
        orarEvent.agileEventName = null;
      }
      else if (orarEvent.deleteAgile) {
        id = orarEvent.agileEventId;
        orarEvent.agileEventName = null;
      }
      else if (orarEvent.deleteWaterfall) {
        id = orarEvent.wateEventId;
        orarEvent.waterfallEventName = null;
      }
    }

    this._eventService.deleteEvent(id).subscribe((data) => {
      // this.orarEvents.splice(this.orarEvents.indexOf(orarEvent), 1);

    }, (error) => {
      console.log(error);
    });
  }


  getOrarEventsByCode(code: string) {
    this.buttonIsClicked = false;
    this._eventService.getOrarEventsByCode(code).subscribe((orarEvents) => {
      console.log("evenimente dupa un cod");
      console.log(orarEvents);
      this.orarEvents = orarEvents;
      if (this.orarEvents.length == 0) {
        this.alertSerice.danger("Cod gresit! Introduceti din nou. ");

      }

    }, (error) => {
      console.log(error);
    })
  }


  }

