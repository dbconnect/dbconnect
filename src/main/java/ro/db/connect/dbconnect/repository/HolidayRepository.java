package ro.db.connect.dbconnect.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ro.db.connect.dbconnect.domain.Event;
import ro.db.connect.dbconnect.domain.Holiday;

import java.util.List;

@Repository
@Transactional(readOnly = true)
public interface HolidayRepository extends JpaRepository<Holiday, Long> {

    //Cauta o sarbatoare dupa an,luna si zi
    @Query(value = "SELECT * FROM holiday WHERE year(date) = ?1 AND MONTH(date) = ?2 AND DAY(date) = ?3", nativeQuery = true)
    List<Holiday> findHolidays(Integer year,Integer month, Integer day);

}
