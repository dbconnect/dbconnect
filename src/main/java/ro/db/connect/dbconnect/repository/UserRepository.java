package ro.db.connect.dbconnect.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ro.db.connect.dbconnect.domain.Event;
import ro.db.connect.dbconnect.domain.User;

@Repository
@Transactional(readOnly = true)
public interface UserRepository extends JpaRepository<User, Long>
{

    //Cauta un user dupa cod
    User findByCod(Long cod);


}
