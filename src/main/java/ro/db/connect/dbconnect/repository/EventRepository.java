package ro.db.connect.dbconnect.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ro.db.connect.dbconnect.domain.Event;

import java.util.List;

@Repository
@Transactional(readOnly = true)
public interface EventRepository extends JpaRepository<Event, Long> {


    List<Event> findByName(String name);

    //Cauta un event dupa an,luna si zi
    @Query(value = "SELECT * FROM event WHERE year(data) = ?1 AND MONTH(data) = ?2 AND DAY(data) = ?3", nativeQuery = true)
    List<Event> findEventsByData(Integer year,Integer month, Integer day);

    @Query(value = "SELECT h.name FROM holiday h WHERE year(date) = ?1 AND MONTH(date) = ?2 AND DAY(date) = ?3 ", nativeQuery = true)
    String findHoliday(Integer year,Integer month, Integer day);

//    @Modifying
//    @Transactional
//    @Query("delete from  Event e where e.id = ?1")
//    void deleteEventById(Long id);

    void deleteEventById(Long id);
    Event findEventById(Long id);
}
