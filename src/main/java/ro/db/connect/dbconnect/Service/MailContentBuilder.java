package ro.db.connect.dbconnect.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

@Service
public class MailContentBuilder {

    private TemplateEngine templateEngine;

    @Autowired
    public MailContentBuilder(TemplateEngine templateEngine) {
        this.templateEngine = templateEngine;
    }

    public String build(String header,String text) {
        Context context = new Context();
        context.setVariable("accept", header);
        context.setVariable("text",text);
        return templateEngine.process("mailTemplate", context);
    }
    public String buildFormatError(String text1,String text2,String format,String exemplu, String exempluBold) {
        Context context = new Context();
        context.setVariable("text1",text1);
        context.setVariable("text2",text2);
        context.setVariable("format", format);
        context.setVariable("exemplu",exemplu);
        context.setVariable("exempluBold",exempluBold);
        return templateEngine.process("mailFormatError", context);
    }

    public String buildFormatSuccess(String text1, String data, String ora_start, String ora_end, String token) {
        Context context = new Context();
        context.setVariable("text1",text1);
        context.setVariable("data", data);
        context.setVariable("ora_start",ora_start);
        context.setVariable("ora_end",ora_end);
        context.setVariable("token", token);
        return templateEngine.process("mailAccept", context);
    }

}