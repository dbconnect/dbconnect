package ro.db.connect.dbconnect.security;

import ro.db.connect.dbconnect.domain.Event;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class Code {


    public static String codeEvent(Event event) {
        char[] pass;
        StringBuilder password = new StringBuilder()
                .append(event.getData().get(Calendar.YEAR));

        if (event.getData().get(Calendar.MONTH) < 10) {
            password.append(0)
                    .append(event.getData().get(Calendar.MONTH)+1);
        } else {
            password.append(event.getData().get(Calendar.MONTH)+1);
        }

        if (event.getData().get(Calendar.DAY_OF_MONTH) < 10) {
            password.append(0).append(event.getData().get(Calendar.DAY_OF_MONTH));
        } else {
            password.append(event.getData().get(Calendar.DAY_OF_MONTH));
        }

        if (event.getOra_start() < 10) {
            password.append(0).append(event.getOra_start());
        } else {
            password.append(event.getOra_start());
        }
        if (event.getOra_end() < 10) {
            password.append(0).append(event.getOra_end());
        } else {
            password.append(event.getOra_end());
        }

        if (event.getAgile()) password.append(1);
        else password.append(0);

        if (event.getWaterfall()) password.append(1);
        else password.append(0);


        pass = password.toString().toCharArray();
        for (int i = 0; i < pass.length; i++) {
            pass[i] += 17;
            if (i % 2 == 1) {
                pass[i] += 8;
            }
            if (i % 4 == 0) {
                pass[i] -= 12;
            }

        }

        return String.valueOf(pass);
    }

    public static Event decodeEvent(String code) {
        char[] pass = code.toCharArray();

        for (int i = 0; i < pass.length; i++) {
            pass[i] -= 17;
            if (i % 2 == 1) {
                pass[i] -= 8;
            }
            if (i % 4 == 0) {
                pass[i] += 12;
            }
        }
        String password = String.valueOf(pass);
        if (!password.matches("[0-9]+")){
            return null;
        }

        Calendar data = new GregorianCalendar();
        int an = Integer.valueOf(password.substring(0,4));
        if (an < Calendar.getInstance().get(Calendar.YEAR)) {
            return null;
        }
        int luna = Integer.valueOf(password.substring(4,6));
        if (luna < Calendar.getInstance().get(Calendar.MONTH) || luna > 12 || luna < 0) {
            return null;
        }
        int zi = Integer.valueOf(password.substring(6,8));
//        if (zi < Calendar.getInstance().get(Calendar.DAY_OF_MONTH) || zi > 31 || zi < 0) {
//            return null;
//        }
        data.set(an,luna, zi);

        int oraStart = Integer.valueOf(password.substring(8,10));
        if (oraStart < 9 || oraStart > 20) {
            return null;
        }
        int oraEnd = Integer.valueOf(password.substring(10,12));
        if (oraEnd < 10 || oraEnd > 21) {
            return null;
        }

        if (!password.substring(12,13).equals("1") )
            if(!password.substring(12,13).equals("0"))
                return null;
        if(!password.substring(13,14).equals("1"))
            if(!password.substring(13,14).equals("0"))
                return null;

        boolean agile = false;
        if (password.substring(12,13).equals("1")) agile = true;

        boolean waterfall = false;
        if (password.substring(13,14).equals("1")) waterfall = true;

        Event ev = new Event();
        ev.setAgile(agile);
        ev.setWaterfall(waterfall);
        ev.setData(data);
        ev.setOra_start(oraStart);
        ev.setOra_end(oraEnd);
        return ev;
    }




}

