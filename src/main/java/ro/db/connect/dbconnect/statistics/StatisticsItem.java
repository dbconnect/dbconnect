package ro.db.connect.dbconnect.statistics;

public class StatisticsItem {

    private String label;
    private Integer value;

    public StatisticsItem() {
    }

    public StatisticsItem(String label, Integer value) {
        if (label != null)
            this.label = label;
        else
            throw new IllegalArgumentException("Label null");
        if (value != null)
            this.value = value;
        else
            throw new IllegalArgumentException("Value null");
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {

        if (label != null)
            this.label = label;
        else
            throw new IllegalArgumentException("NULL");
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        if (value != null)
            this.value = value;
        else
            throw new IllegalArgumentException("NULL");
    }

    @Override
    public String toString() {
        return "StatisticsItem{" +
                "label='" + label + '\'' +
                ", value=" + value +
                '}';
    }
}
