package ro.db.connect.dbconnect;

import it.ozimov.springboot.mail.configuration.EnableEmailTools;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.integration.config.EnableIntegration;
import ro.db.connect.dbconnect.mail.IMAPIdleConfiguration;

import javax.annotation.PostConstruct;

@SpringBootApplication
@ComponentScan("ro.db.connect.dbconnect")
@EnableEmailTools
@EnableIntegration
@EnableJpaRepositories("ro.db.connect.dbconnect.repository")
public class DbConnectApplication {


    @Autowired
    private IMAPIdleConfiguration imapIdleConfiguration;

	public static void main(String[] args) {
		SpringApplication.run(DbConnectApplication.class, args);
	}

    @PostConstruct
    public void startMailListener() {

        imapIdleConfiguration.startIdleChannel();
    }
}
