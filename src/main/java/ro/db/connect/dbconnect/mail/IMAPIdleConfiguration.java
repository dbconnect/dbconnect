package ro.db.connect.dbconnect.mail;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.mail.ImapIdleChannelAdapter;
import org.springframework.integration.mail.ImapMailReceiver;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHandler;
import org.springframework.messaging.MessagingException;
import ro.db.connect.dbconnect.Service.MailContentBuilder;
import ro.db.connect.dbconnect.domain.Event;
import ro.db.connect.dbconnect.repository.EventRepository;
import ro.db.connect.dbconnect.security.Code;

import javax.annotation.PostConstruct;
import javax.mail.BodyPart;
import javax.mail.Folder;
import javax.mail.internet.MimeMultipart;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.util.Calendar;
import java.util.Properties;

@Configuration
public class IMAPIdleConfiguration {

    private final TestService testService;

    @Autowired
    private MailContentBuilder mailContentBuilder;

    @Autowired
    private MailParser mailParser;

    @Autowired
    private EventRepository eventRepository;

    @Autowired
    public IMAPIdleConfiguration(TestService testService) {
        this.testService = testService;
    }

    private Properties javaMailProperties() {
        Properties javaMailProperties = new Properties();

        javaMailProperties.setProperty("mail.imap.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        javaMailProperties.setProperty("mail.imap.socketFactory.fallback", "false");
        javaMailProperties.setProperty("mail.store.protocol", "imaps");
        javaMailProperties.setProperty("mail.debug", "true");
        javaMailProperties.setProperty("mail.smtp.timeout", "10000");
        javaMailProperties.setProperty("mail.imaps.ssl.trust", "*");
        javaMailProperties.setProperty("mail.imap.connectionpoolsize","10");
        return javaMailProperties;
    }

    @Bean
    public ImapMailReceiver imapMailReceiver() {
        return new ImapMailReceiver("imaps://dbconnectreservation:parolaconnect@imap.gmail.com:993/inbox");
    }

    @Bean
    public ImapIdleChannelAdapter imapIdleChannelAdapter(ImapMailReceiver mailReceiver) {
        return new ImapIdleChannelAdapter(mailReceiver);
    }

    @PostConstruct
    ImapIdleChannelAdapter mailAdapter() {

        ImapMailReceiver mailReceiver = imapMailReceiver();
        mailReceiver.setJavaMailProperties(javaMailProperties());
        mailReceiver.setShouldDeleteMessages(false);
        mailReceiver.setShouldMarkMessagesAsRead(true);

        mailReceiver.afterPropertiesSet();

        ImapIdleChannelAdapter imapIdleChannelAdapter = imapIdleChannelAdapter(mailReceiver);
        imapIdleChannelAdapter.setAutoStartup(true);
        imapIdleChannelAdapter.setOutputChannel(directChannel());

        imapIdleChannelAdapter.afterPropertiesSet();

        return imapIdleChannelAdapter;

    }

    @Bean
    public DirectChannel directChannel() {
        return new DirectChannel();
    }

    @ServiceActivator
    public void startIdleChannel() {
        DirectChannel inputChannel = this.directChannel();
        inputChannel.subscribe(new MessageHandler() {
            @Override
            public void handleMessage(Message<?> message) throws MessagingException {

                javax.mail.Message mailMessage = (javax.mail.Message) message.getPayload();

                try {
                    System.out.println("----------------------");
                    Folder folder = mailMessage.getFolder();
                    folder.open(Folder.READ_ONLY);
                    javax.mail.Message message1 = folder.getMessage(folder.getMessageCount());
                    System.out.println(message1.getContent());
                    MimeMultipart mimeMultipart = (MimeMultipart) message1.getContent();
                    BodyPart bodyPart = mimeMultipart.getBodyPart(mimeMultipart.getCount() - 1);

                    String mailText = "";
                    if (bodyPart.isMimeType("text/plain"))
                        mailText = (String) bodyPart.getContent();
                    else if (bodyPart.isMimeType("text/html"))
                        mailText = org.jsoup.Jsoup.parse((String) bodyPart.getContent()).text();

                    System.out.println("!!!!!!!!!!!!@@@@@@@@@@@@" + mailText);
                    String from = String.valueOf(mailMessage.getFrom()[0]);
                    System.out.println("Ai primit un mail de la : " + getSender(from));
                    folder.close(false);
                    System.out.println("----------------------");

                    //Chech if the text received is valid
                    if (!MailParser.isValid(mailText)) {
                        String html = mailContentBuilder.buildFormatError("Formatul introdus nu este valid.",
                                "Formatul este de forma:", "[Nume eveniment] [Numar participanti] [Data] [Intervalul orar]",
                                "Exemplu: ", "JavaSchool 75 20/06/2018 10-12 ");
//
                        //Daca nu respecta formatul trimitem mail inapoi si explicam formatul cerut utilizatorului
                        try {
                            testService.sendHtmlEmail(getSender(from), "Rezervarea nu a fost acceptata", html);
                        } catch (javax.mail.MessagingException e) {
                            e.printStackTrace();
                        }

                    } else {
                        //Verificam daca este disponibila ziua si ora pentru a salva evenimentul
                        Event event = mailParser.isAvailable(mailText);
                        if (event == null) {
                            String html = mailContentBuilder.build("Salile sunt deja rezervate la ora ceruta de dumneavoastra.",
                                    "Va recomandam sa intrati pe pagina noastra web pentru a verifica disponibilitatea.");
                            //Evenimentul nu este disponibil. Trimitem mail inapoi
                            try {
                                testService.sendHtmlEmail(getSender(from), "Rezervarea nu a fost acceptata", html);
                            } catch (javax.mail.MessagingException e) {
                                e.printStackTrace();
                            }

                        } else {
                            if (event.getName().equals("Eroare:Este_Sarbatoare!")) {

                                String html = mailContentBuilder.build("Ziua aleasa reprezinta o sarbatoare legala.",
                                        "Va asteptam in orice alta zi lucratoare!");

                                //Evenimentul nu a fost rezervat pentru ca este sarbatoare
                                try {
                                    testService.sendHtmlEmail(getSender(from), "Rezervarea nu a fost acceptata", html);
                                } catch (javax.mail.MessagingException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                if (event.getName().equals("Eroare:Este_weekend!")) {

                                    String html = mailContentBuilder.build("Ziua aleasa reprezinta o zi de weekend.",
                                            "Va asteptam in orice alta zi lucratoare!");

                                    //Evenimentul nu a fost rezervat pentru ca este weekend
                                    try {
                                        testService.sendHtmlEmail(getSender(from), "Rezervarea nu a fost acceptata", html);
                                    } catch (javax.mail.MessagingException e) {
                                        e.printStackTrace();
                                    }
                                } else {
                                    eventRepository.save(event);
                                    int luna = event.getData().get(Calendar.MONTH) + 1;
                                    String data = "" + event.getData().get(Calendar.DAY_OF_MONTH) + "/" + luna +
                                            "/" + event.getData().get(Calendar.YEAR);
                                    String token = Code.codeEvent(event);
                                    String html = mailContentBuilder.buildFormatSuccess("Rezervarea a fost facuta cu succes.", data,
                                            event.getOra_start().toString(), event.getOra_end().toString(), token);

                                    //Evenimentul a fost rezervat cu succes
                                    try {
                                        testService.sendHtmlEmail(getSender(from), "Rezervarea a fost acceptata", html);
                                    } catch (javax.mail.MessagingException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        }
                    }


                } catch (javax.mail.MessagingException | IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private String getSender(@NotNull String mail) {
        int start = mail.indexOf('<');
        int end = mail.indexOf('>');
        return mail.substring(start + 1, end);
    }

}
