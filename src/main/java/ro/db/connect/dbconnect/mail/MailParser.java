package ro.db.connect.dbconnect.mail;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.db.connect.dbconnect.domain.Event;
import ro.db.connect.dbconnect.domain.Holiday;
import ro.db.connect.dbconnect.repository.EventRepository;
import ro.db.connect.dbconnect.repository.HolidayRepository;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.StringTokenizer;

@Component
public class MailParser {

    private static final int START_OF_DAY = 9;
    private static final int END_OF_DAY = 21;
    @Autowired
    private EventRepository eventRepository;
    @Autowired
    private HolidayRepository holidayRepository;



    public MailParser() {
    }

    public static boolean isValid(String mail) {
        if (mail == null || mail.equals(""))
            return false;

        StringTokenizer stringTokenizer = new StringTokenizer(mail, " ");
        ArrayList<String> tokens = new ArrayList<>();

        while( stringTokenizer.hasMoreTokens()) {
            tokens.add(stringTokenizer.nextToken());
        }
        if (tokens.size() != 4)
            return false;

        if (!tokens.get(1).matches("[0-9]+"))
            return false;
        if(Integer.parseInt(tokens.get(1))<=0 || Integer.parseInt(tokens.get(1))>100)
            return false;
        //tokens.get(2) = 14/04/2018

        StringTokenizer stringTokenizerDate = new StringTokenizer(tokens.get(2),"/");
        ArrayList<Integer>data = new ArrayList<>();
        String token;
        while(stringTokenizerDate.hasMoreTokens())
        {
            token = stringTokenizerDate.nextToken();
            if(!token.matches("[0-9]+"))
                return false;
            data.add(Integer.parseInt(token));
        }
        if(data.size()!=3)
            return false;
        //Data introdusa nu este valida
        if (data.get(0)<=0 || data.get(0)>31 || data.get(1)<=0 || data.get(1)>12 || data.get(2)< LocalDateTime.now().getYear())
            return false;

        System.out.println(data.get(0)+" "+data.get(1)+" "+data.get(2)+" ");

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, data.get(2));

        // We will have to increment the month field by 1

        calendar.set(Calendar.MONTH, data.get(1)-1);

        // As the month indexing starts with 0

        calendar.set(Calendar.DAY_OF_MONTH, data.get(0));

        //System.out.println("Data din calendar este "+ calendar.get(Calendar.DAY_OF_WEEK) + " "+Calendar.SUNDAY+" "+ Calendar.SATURDAY);

        if(  calendar.get(Calendar.YEAR) < LocalDateTime.now().getYear() ) {
            return false;
        }

        String start;
        String end;

        StringTokenizer stInt = new StringTokenizer(tokens.get(3), "-");

        if(stInt.countTokens()!=2)
            return false;

        start = stInt.nextToken();
        end = stInt.nextToken();

//        start = Integer.parseInt(stInt.nextToken());
//        end = Integer.parseInt(stInt.nextToken());
        if( !start.matches("[0-9]+") || !end.matches("[0-9]+"))
            return false;
        return Integer.parseInt(start) >= MailParser.START_OF_DAY && Integer.parseInt(end) <= MailParser.END_OF_DAY;
    }

    @Autowired
    public void setEventRepository(EventRepository eventRepository) {
        this.eventRepository = eventRepository;
    }

    public Event isAvailable(String mail) {

        Event event = new Event();
        StringTokenizer st = new StringTokenizer(mail, " ");
        ArrayList<String> tk = new ArrayList<>();

        while( st.hasMoreTokens()) {
            tk.add(st.nextToken());
        }

        StringTokenizer stringTokenizerDate = new StringTokenizer(tk.get(2),"/");
        ArrayList<Integer>data = new ArrayList<>();
        while(stringTokenizerDate.hasMoreTokens())
        {
            data.add(Integer.parseInt(stringTokenizerDate.nextToken()));
        }
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, data.get(2));

        // We will have to increment the month field by 1

        calendar.set(Calendar.MONTH, data.get(1)-1);

        // As the month indexing starts with 0

        calendar.set(Calendar.DAY_OF_MONTH, data.get(0));

        int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
        if( dayOfWeek == Calendar.SATURDAY ||dayOfWeek == Calendar.SUNDAY) {
            event.setName("Eroare:Este_weekend!");
            return event;
        }else{
            //Caut sarbatoare in ziua din mail
            List<Holiday> sarbatoare = holidayRepository.findHolidays(data.get(2), data.get(1), data.get(0));

            if (sarbatoare.size() == 0) {


                //Iau evenimentele din ziua data in email
                List<Event> evenimenteDupaZi = eventRepository.findEventsByData(data.get(2), data.get(1), data.get(0));


                Boolean[][] tabelOcupare = new Boolean[2][13];

                Boolean eventAgile = Boolean.FALSE,
                        eventWaterfall = Boolean.FALSE,
                        eventValidat;

                //Ziua este goala si se poate pune evenimentul la ora dorita
                if (evenimenteDupaZi.size() == 0) {
                    if (Integer.parseInt(tk.get(1)) < 50) {
                        eventAgile = Boolean.FALSE;
                        eventWaterfall = Boolean.TRUE;
                    } else {
                        eventAgile = Boolean.TRUE;
                        eventWaterfall = Boolean.TRUE;
                    }

                    event.setName(tk.get(0));
                    if (eventAgile == Boolean.FALSE) {
                        event.setAgile(false);
                    } else {
                        event.setAgile(true);
                    }
                    event.setWaterfall(true);

                    //Ora de start si de end din email
                    StringTokenizer stInt = new StringTokenizer(tk.get(3), "-");
                    Integer start = Integer.parseInt(stInt.nextToken());
                    Integer end = Integer.parseInt(stInt.nextToken());

                    event.setOra_start(start);
                    event.setOra_end(end);
                    event.setData(calendar);
//                    eventRepository.save(event);
                    return event;
                }
                for (int i = 0; i < 13; i++) {
                    tabelOcupare[0][i] = Boolean.FALSE;
                    tabelOcupare[1][i] = Boolean.FALSE;
                }
                //completare matrice cu orele corespunzatoare
                for (Event ev : evenimenteDupaZi) {
                    //int durata = ev.getOra_end() - ev.getOra_start();
                    int s = ev.getOra_start() - 9;
                    int e = ev.getOra_end() - 9;
                    for (int i = s; i < e; i++) {
                        if (ev.getWaterfall()) {
                            tabelOcupare[0][i] = Boolean.TRUE;
                        }
                        if (ev.getAgile()) {
                            tabelOcupare[1][i] = Boolean.TRUE;
                        }
                    }
                }

                eventWaterfall = Boolean.TRUE;
                eventAgile = Boolean.TRUE;

                //Ora de start si de end din email
                StringTokenizer stInt = new StringTokenizer(tk.get(3), "-");
                Integer start = Integer.parseInt(stInt.nextToken());
                Integer end = Integer.parseInt(stInt.nextToken());

                for (int i = start; i < end; i++) {
                    if ((!tabelOcupare[0][i - 9]) && (eventWaterfall == Boolean.TRUE)) {
                        eventWaterfall = Boolean.TRUE;
                    } else {
                        eventWaterfall = Boolean.FALSE;
                    }

                    if ((!tabelOcupare[1][i - 9]) && (eventAgile == Boolean.TRUE)) {
                        eventAgile = Boolean.TRUE;
                    } else {
                        eventAgile = Boolean.FALSE;
                    }
                }

                if (Integer.parseInt(tk.get(1)) < 50) {
                    if (eventWaterfall == Boolean.TRUE) {
                        eventAgile = Boolean.FALSE;
                        eventValidat = Boolean.TRUE;
                    } else if (eventAgile == Boolean.TRUE) {
                        eventValidat = Boolean.TRUE;
                    } else {
                        eventValidat = Boolean.FALSE;
                    }
                } else {
                    if (eventWaterfall == Boolean.TRUE && eventAgile == Boolean.TRUE) {
                        eventValidat = Boolean.TRUE;
                    } else eventValidat = Boolean.FALSE;
                }

                if (eventValidat == Boolean.TRUE) {
                    //Setam numele
                    event.setName(tk.get(0));
                    //Setam salile
                    if (eventAgile != Boolean.TRUE) {
                        event.setAgile(false);
                    } else {
                        event.setAgile(true);
                    }
                    if (eventWaterfall != Boolean.TRUE) {
                        event.setWaterfall(false);
                    } else {
                        event.setWaterfall(true);
                    }
                    //Setam ora si data
                    event.setOra_start(start);
                    event.setOra_end(end);
                    event.setData(calendar);
                    //Salvam event-ul in baza de date
                    eventRepository.save(event);
                    return event;
                }
            } else {
                event.setName("Eroare:Este_Sarbatoare!");
                return event;
            }
        }
        return null;
    }

}
