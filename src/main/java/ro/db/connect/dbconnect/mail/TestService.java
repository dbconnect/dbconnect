package ro.db.connect.dbconnect.mail;

import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

@Service
public class TestService {

    public void sendHtmlEmail(String toAddress, String subject, String textMail)
            throws MessagingException {

        Properties props = new Properties();
        props.setProperty("mail.transport.protocol", "smtp");
        props.setProperty("mail.host", "smtp.gmail.com");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "465");
        props.put("mail.debug", "true");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class","javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.socketFactory.fallback", "false");
        Session session = Session.getDefaultInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication("dbconnectreservation@gmail.com","parolaconnect");
                    }
                });

        Transport transport = session.getTransport();
        InternetAddress addressFrom = new InternetAddress("dbconnectreservation@gmail.com");



        MimeMessage message = new MimeMessage(session);
        MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(message);
        message.setContent(textMail,"text/html");
        message.setSender(addressFrom);
        message.setSubject(subject);
        message.addRecipient(Message.RecipientType.TO, new InternetAddress(toAddress));

        transport.connect();
        Transport.send(message);
        transport.close();
    }
}