package ro.db.connect.dbconnect.domain;

import javax.persistence.*;
import java.util.List;

@Entity
public class User
{
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = "secventaUser",sequenceName = "secventaUser",initialValue = 18531,allocationSize = 9)
    private Long cod;
    private String mail;

    @OneToMany
    private List<Event> events;

    public Long getCod() {
        return cod;
    }

    public void setCod(Long cod) {
        this.cod = cod;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }
}
