package ro.db.connect.dbconnect.domain;

import javax.persistence.*;
import java.util.Calendar;

@Entity
public class Event
{
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = "secventaEvent", sequenceName = "secventaEvent", initialValue = 1, allocationSize = 1)
    private Long id;
    private String name;
    private Integer numberOfParticipants;
    private Integer ora_start;
    private Integer ora_end;
    private Calendar data;
    private Boolean agile;
    private Boolean waterfall;

    public Event(String name, Integer ora_start, Integer ora_end, Calendar data, Boolean agile, Boolean waterfall) {
        this.name = name;
        this.ora_start = ora_start;
        this.ora_end = ora_end;
        this.data = data;
        this.agile = agile;
        this.waterfall = waterfall;
    }

    public Event() {
    }

    @Override
    public String toString() {
        return "Event{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", numberOfParticipants=" + numberOfParticipants +
                ", ora_start=" + ora_start +
                ", ora_end=" + ora_end +
                ", data=" + data +
                ", agile=" + agile +
                ", waterfall=" + waterfall +
                '}';
    }

    public Boolean getAgile() {
        return agile;
    }

    public void setAgile(Boolean agile) {
        this.agile = agile;
    }

    public Boolean getWaterfall() {
        return waterfall;
    }

    public void setWaterfall(Boolean waterfall) {
        this.waterfall = waterfall;
    }

    public Integer getOra_start() {
        return ora_start;
    }

    public void setOra_start(Integer ora_start) {
        this.ora_start = ora_start;
    }

    public Integer getOra_end() {
        return ora_end;
    }

    public void setOra_end(Integer ora_end) {
        this.ora_end = ora_end;
    }

    public Calendar getData() {
        return data;
    }

    public void setData(Calendar data) {
        this.data = data;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getNumberOfParticipants() {
        return numberOfParticipants;
    }

    public void setNumberOfParticipants(Integer numberOfParticipants) {
        this.numberOfParticipants = numberOfParticipants;
    }
}
