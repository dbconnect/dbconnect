package ro.db.connect.dbconnect.domain;

import javax.persistence.*;
import java.util.Calendar;
import java.util.Date;

@Entity
public class Holiday
{

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = "secventaHoliday",sequenceName = "secventaHoliday",initialValue = 1,allocationSize = 1)
    private Integer id;

    private Calendar date;
    private String name;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Calendar getDate() {
        return date;
    }

    public void setDate(Calendar date) {
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
