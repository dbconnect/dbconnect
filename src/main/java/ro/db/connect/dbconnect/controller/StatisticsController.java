package ro.db.connect.dbconnect.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ro.db.connect.dbconnect.domain.Event;
import ro.db.connect.dbconnect.repository.EventRepository;
import ro.db.connect.dbconnect.statistics.StatisticsItem;

import java.util.*;

@RestController
@CrossOrigin(origins="http://localhost:4200", allowedHeaders="*")
@RequestMapping("/api1")
public class StatisticsController {

    private static List<StatisticsItem> statistics;
    private static List<Integer> months;
    private static Map<Integer, String> monthsMap;
    private static List<Integer> daysOfWeekList;
    private static Map<Integer, String> dayOfWeeknMap;

    @Autowired
    private EventRepository eventRepository;

    @RequestMapping(value = "statistics1", method = RequestMethod.GET)
    @ResponseBody
    public List<Event> getEvents() {
        return eventRepository.findAll();
    }

    public List<StatisticsItem> getTheMostPopularHourByMonthByRoom(Boolean isAgile, Boolean isWaterfall) throws NullPointerException {

        statistics = new ArrayList<>();

        init();

        List<Event> events = eventRepository.findAll();

        Collections.sort(events, new Comparator<Event>() {
            @Override
            public int compare(Event o1, Event o2) {
                String monthO1 = String.valueOf(o1.getData().get(Calendar.MONTH));
                String monthO2 = String.valueOf(o2.getData().get(Calendar.MONTH));
                return monthO1.compareTo(monthO2);
            }
        });

        for(Event ev: events)
            System.out.println(ev.getData().get(Calendar.MONTH)+ " "+ev);

        for (Integer month : months) {
            int popularHour = 0, maxHourByMonth = 0;
            for (int i = 9; i <= 20; i++) {
                int counter = 0;
                for (Event ev : events) {
                    if (ev.getAgile() == isAgile && ev.getWaterfall() == isWaterfall)
                        if ((ev.getOra_start() <= i && ev.getOra_end() >= i)
                                && month.equals(ev.getData().get(Calendar.MONTH)))
                            counter++;
                }

                if (counter > maxHourByMonth) {
                    maxHourByMonth = counter;
                    popularHour = i;
                }
            }

            statistics.add(new StatisticsItem(monthsMap.get(month), popularHour));
        }

        return statistics;

    }

    @GetMapping("/statistics")
    @ResponseBody
    public List<StatisticsItem> getTheMostPopularHourByMonthAgile() {

        statistics = getTheMostPopularHourByMonthByRoom(true, false);

        System.out.println("Statistics val:");
        for(StatisticsItem val: statistics){
            System.out.println(val);
        }
        return statistics;
    }

    @GetMapping("/statistics/11")
    @ResponseBody
    public List<StatisticsItem> getTheMostPopularHourByMonthWaterfall() {

        statistics = getTheMostPopularHourByMonthByRoom(false, true);

        System.out.println("Statistics val:");
        for (StatisticsItem val : statistics) {
            System.out.println(val);
        }
        return statistics;
    }

    @GetMapping("/statistics/12")
    @ResponseBody
    public List<StatisticsItem> getTheMostPopularHourByMonthAgileAndWaterfall() {

        statistics = getTheMostPopularHourByMonthByRoom(true, true);

        System.out.println("Statistics val:");
        for (StatisticsItem val : statistics) {
            System.out.println(val);
        }
        return statistics;
    }

    public List<StatisticsItem> getTOtalHoursOfMonthByRoom(Boolean isAgile, Boolean isWaterfall) {
    // media numarului de ore ocupate pe luna
        statistics = new ArrayList<>();

        init();

        List<Event> events = eventRepository.findAll();
        Collections.sort(events, new Comparator<Event>() {
            @Override
            public int compare(Event o1, Event o2) {
                String monthO1 = String.valueOf(o1.getData().get(Calendar.MONTH));
                String monthO2 = String.valueOf(o2.getData().get(Calendar.MONTH));
                return monthO1.compareTo(monthO2);
            }
        });

        for(Event ev: events)
            System.out.println(ev.getData().get(Calendar.MONTH)+ " "+ev);

        for(Integer month:months){
            int counterNo = 0, totalHour = 0;
            for(Event ev:events){
                if (ev.getAgile() == isAgile && ev.getWaterfall() == isWaterfall)
                    if (month == (ev.getData().get(Calendar.MONTH))) {
                        counterNo++;
                        totalHour += (ev.getOra_end() - ev.getOra_start());
                    }
            }
            if(counterNo > 0)
                statistics.add(new StatisticsItem(monthsMap.get(month), totalHour/counterNo));
            else
                statistics.add(new StatisticsItem(monthsMap.get(month), totalHour));
        }

        return statistics;
    }

    @GetMapping("/statistics/2")
    @ResponseBody
    public List<StatisticsItem> getTheTotalHoursOfAMonthAgile() {

        statistics = getTOtalHoursOfMonthByRoom(true, false);

        System.out.println("Statistics val:");
        for (StatisticsItem val : statistics) {
            System.out.println(val);
        }
        return statistics;
    }

    @GetMapping("/statistics/22")
    @ResponseBody
    public List<StatisticsItem> getTheTotalHoursOfAMonthWaterfall() {

        statistics = getTOtalHoursOfMonthByRoom(false, true);

        System.out.println("Statistics val:");
        for (StatisticsItem val : statistics) {
            System.out.println(val);
        }
        return statistics;
    }

    @GetMapping("/statistics/23")
    @ResponseBody
    public List<StatisticsItem> getTheTotalHoursOfAMonthAgileAndWaterfall() {

        statistics = getTOtalHoursOfMonthByRoom(true, true);

        System.out.println("Statistics val:");
        for(StatisticsItem val: statistics){
            System.out.println(val);
        }
        return statistics;
    }


    //BIANCA
    public static Map<String, Integer> daysOfWeek;
    public static List<StatisticsItem> busyHoursPerDaysOfWeek;
    public static List<StatisticsItem> eventsPerPeriod;

    public void init(){
        daysOfWeek = new HashMap<>();
        daysOfWeek.put("Monday", 0);
        daysOfWeek.put("Thursday", 0);
        daysOfWeek.put("Wednesday", 0);
        daysOfWeek.put("Tuesday", 0);
        daysOfWeek.put("Friday", 0);
        daysOfWeek.put("n/a", 0);

        daysOfWeekList = Arrays.asList(0, 1, 2, 3, 4);

        monthsMap = new HashMap<>();
        monthsMap.put(0,"Jan");
        monthsMap.put(1,"Feb");
        monthsMap.put(2,"Mar");
        monthsMap.put(3,"Apr");
        monthsMap.put(4,"May");
        monthsMap.put(5,"Jun");
        monthsMap.put(6,"Jul");
        monthsMap.put(7,"Aug");
        monthsMap.put(8,"Sep");
        monthsMap.put(9,"Oct");
        monthsMap.put(10,"Nov");
        monthsMap.put(11,"Dec");
        months = Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11);

    }

    public List<StatisticsItem> funcStat3(Boolean agile, Boolean waterfall) {

        List<Event> events = eventRepository.findAll();

        init();

        busyHoursPerDaysOfWeek = new ArrayList<>();
        eventsPerPeriod = new ArrayList<>();

        int duration = 0, mon = 0, tue = 0, wed = 0, thu = 0, fri = 0;
        for (Event event : events) {
            if (event.getWaterfall() == waterfall && event.getAgile() == agile) {
                duration = event.getOra_end() - event.getOra_start();
                Calendar calendar = Calendar.getInstance();
                calendar = event.getData();

                if (calendar.get(Calendar.DAY_OF_WEEK) == 0)
                {
                    daysOfWeek.put("Monday", daysOfWeek.get("Monday") + duration);
                    mon++;
                }
                else if (calendar.get(Calendar.DAY_OF_WEEK) == 1)
                {
                    daysOfWeek.put("Tuesday", daysOfWeek.get("Tuesday") + duration);
                    tue++;
                }
                else if (calendar.get(Calendar.DAY_OF_WEEK) == 2) {
                    daysOfWeek.put("Wednesday", daysOfWeek.get("Tuesday") + duration);
                    wed++;
                }
                else if (calendar.get(Calendar.DAY_OF_WEEK) == 3) {
                    daysOfWeek.put("Thursday", daysOfWeek.get("Wednesday") + duration);
                    thu++;
                }
                else if (calendar.get(Calendar.DAY_OF_WEEK) == 4) {
                    daysOfWeek.put("Friday", daysOfWeek.get("Friday") + duration);
                    fri++;
                }
                else
                    daysOfWeek.put("n/a", daysOfWeek.get("n/a") + 1);
            }
        }

        busyHoursPerDaysOfWeek.add(new StatisticsItem("Monday", mon != 0 ? daysOfWeek.get("Monday") / mon : daysOfWeek.get("Monday")));
        busyHoursPerDaysOfWeek.add(new StatisticsItem("Tuesday", tue != 0 ? daysOfWeek.get("Tuesday") / tue : daysOfWeek.get("Tuesday")));
        busyHoursPerDaysOfWeek.add(new StatisticsItem("Wednesday", wed != 0 ? daysOfWeek.get("Wednesday") / wed : daysOfWeek.get("Wednesday")));
        busyHoursPerDaysOfWeek.add(new StatisticsItem("Thursday", thu != 0 ? daysOfWeek.get("Thursday") / thu : daysOfWeek.get("Thursday")));
        busyHoursPerDaysOfWeek.add(new StatisticsItem("Friday", fri != 0 ? daysOfWeek.get("Friday") / fri : daysOfWeek.get("Friday")));


        return busyHoursPerDaysOfWeek;
    }

    //Cele mai libere zile ale saptamanii
    @GetMapping("/statistics/31")
    @ResponseBody
    public List<StatisticsItem> getLeastBusyHoursByDaysAgile() {

        busyHoursPerDaysOfWeek = funcStat3(true, false);

        return busyHoursPerDaysOfWeek;
    }

    @GetMapping("/statistics/32")
    @ResponseBody
    public List<StatisticsItem> getLeastBusyHoursByDaysWaterfall() {

        busyHoursPerDaysOfWeek = funcStat3(false, true);

        return busyHoursPerDaysOfWeek;
    }

    @GetMapping("/statistics/33")
    @ResponseBody
    public List<StatisticsItem> getLeastBusyHoursByDaysAgileAndWaterfall() {

        busyHoursPerDaysOfWeek = funcStat3(true, true);

        return busyHoursPerDaysOfWeek;
    }

    public List<StatisticsItem> funcStat4(Boolean agile, Boolean waterfall) {
        List<Event> events = eventRepository.findAll();

        int eventsPerFirstPart = 0;
        int eventsPerSecondPart = 0;

        init();

        busyHoursPerDaysOfWeek = new ArrayList<>();
        eventsPerPeriod = new ArrayList<>();

        for (Event event : events) {
            if(event.getWaterfall() == waterfall && event.getAgile()==agile) {
                if (event.getOra_start() > 12 || event.getOra_end() > 12)
                    eventsPerSecondPart += 1;
                else
                    eventsPerFirstPart += 1;
            }
        }

        eventsPerPeriod.add(new StatisticsItem("First part of the day", eventsPerFirstPart));
        eventsPerPeriod.add(new StatisticsItem("Second part of the day", eventsPerSecondPart));

        return eventsPerPeriod;
    }

    //Pie Chart - In which time of the day are the most events ?
    @GetMapping("/statistics/34")
    @ResponseBody
    public List<StatisticsItem> getMostEventsPerPeriodOfDayAgile() {

        eventsPerPeriod = funcStat4( true, false);

        return eventsPerPeriod;
    }

    @GetMapping("/statistics/35")
    @ResponseBody
    public List<StatisticsItem> getMostEventsPerPeriodOfDayWaterfall() {

        eventsPerPeriod = funcStat4(false, true);

        return eventsPerPeriod;
    }

    @GetMapping("/statistics/36")
    @ResponseBody
    public List<StatisticsItem> getMostEventsPerPeriodOfDayAgileAndWaterfall() {

        eventsPerPeriod = funcStat4( true, true);

        return eventsPerPeriod;
    }

    public List<StatisticsItem> getMostPopularHoursByDaysOfWeek(Boolean isAgile, Boolean isWaterfall){
        statistics = new ArrayList<>();

        init();

        dayOfWeeknMap = new HashMap<>();
        dayOfWeeknMap.put(0, "Monday");
        dayOfWeeknMap.put(1, "Thursday");
        dayOfWeeknMap.put(2, "Wednesday");
        dayOfWeeknMap.put(3, "Tuesday");
        dayOfWeeknMap.put(4, "Friday");

        List<Event> events = eventRepository.findAll();

        Collections.sort(events, new Comparator<Event>() {
            @Override
            public int compare(Event o1, Event o2) {
                String monthO1 = String.valueOf(o1.getData().get(Calendar.DAY_OF_WEEK));
                String monthO2 = String.valueOf(o2.getData().get(Calendar.DAY_OF_WEEK));
                return monthO1.compareTo(monthO2);
            }
        });

        for(Event ev: events)
            System.out.println(ev.getData().get(Calendar.MONTH)+ " "+ev);

        for (Integer day : daysOfWeekList) {
            int popularHour = 0, maxHourByMonth = 0;
            for (int i = 9; i <= 20; i++) {
                int counter = 0;
                for (Event ev : events) {
                    if (ev.getAgile() == isAgile && ev.getWaterfall() == isWaterfall)
                        if ((ev.getOra_start() <= i && ev.getOra_end() >= i)
                                && day.equals(ev.getData().get(Calendar.DAY_OF_WEEK)))
                            counter++;
                }

                if (counter > maxHourByMonth) {
                    maxHourByMonth = counter;
                    popularHour = i;
                }
            }

            statistics.add(new StatisticsItem(dayOfWeeknMap.get(day), popularHour));
        }

        return statistics;
    }

    @GetMapping("/statistics/27")
    @ResponseBody
    public List<StatisticsItem> getMostPopularHoursByDaysOfWeekAgile(){
        statistics = getMostPopularHoursByDaysOfWeek(true,false);

        return statistics;
    }

    @GetMapping("/statistics/28")
    @ResponseBody
    public List<StatisticsItem> getMostPopularHoursByDaysOfWeekWaterfall(){
        statistics = getMostPopularHoursByDaysOfWeek(false,true);

        return statistics;
    }

    @GetMapping("/statistics/29")
    @ResponseBody
    public List<StatisticsItem> getMostPopularHoursByDaysOfWeekAgileAndWaterfall(){
        statistics = getMostPopularHoursByDaysOfWeek(true,true);

        return statistics;
    }

    private List<StatisticsItem> leastOccupiedHoursByDay(Boolean isAgile, Boolean isWaterfall) {
        statistics = new ArrayList<>();
        init();

        dayOfWeeknMap = new HashMap<>();
        dayOfWeeknMap.put(0, "Monday");
        dayOfWeeknMap.put(1, "Thursday");
        dayOfWeeknMap.put(2, "Wednesday");
        dayOfWeeknMap.put(3, "Tuesday");
        dayOfWeeknMap.put(4, "Friday");

        List<Event> events = eventRepository.findAll();

        Collections.sort(events, new Comparator<Event>() {
            @Override
            public int compare(Event o1, Event o2) {
                String monthO1 = String.valueOf(o1.getData().get(Calendar.DAY_OF_WEEK));
                String monthO2 = String.valueOf(o2.getData().get(Calendar.DAY_OF_WEEK));
                return monthO1.compareTo(monthO2);
            }
        });

        for (Integer day : daysOfWeekList) {
            int freeHour = 0, minHourByDay = Integer.MAX_VALUE;
            for (int i = 9; i <= 20; i++) {
                int counter = 0;
                for (Event ev : events) {
                    if (ev.getAgile() == isAgile && ev.getWaterfall() == isWaterfall)
                        if ((ev.getOra_start() >= i || ev.getOra_end() <= i)
                                && day.equals(ev.getData().get(Calendar.DAY_OF_WEEK))) {
                            counter++;
                        }
                }

                if (counter < minHourByDay) {
                    minHourByDay = counter;
                    freeHour = i;
                }

            }

            statistics.add(new StatisticsItem(dayOfWeeknMap.get(day), freeHour));
        }

        return statistics;
    }

    @GetMapping("/statistics/50")
    @ResponseBody
    public List<StatisticsItem> leastOccupiedHoursByDayAgile() {
        statistics = leastOccupiedHoursByDay(true, false);

        return statistics;
    }

    @GetMapping("/statistics/51")
    @ResponseBody
    public List<StatisticsItem> leastOccupiedHoursByDayWaterfall() {
        statistics = leastOccupiedHoursByDay(false, true);

        return statistics;
    }

    @GetMapping("/statistics/52")
    @ResponseBody
    public List<StatisticsItem> leastOccupiedHoursByDayAgileAndWaterfall() {
        statistics = leastOccupiedHoursByDay(true, true);

        return statistics;
    }
}
