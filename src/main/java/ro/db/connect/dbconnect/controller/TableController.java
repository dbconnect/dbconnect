package ro.db.connect.dbconnect.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ro.db.connect.dbconnect.domain.Event;
import ro.db.connect.dbconnect.orarEvenimente.Orar;
import ro.db.connect.dbconnect.orarEvenimente.OrarService;
import ro.db.connect.dbconnect.repository.EventRepository;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "http://localhost:4200", allowedHeaders = "*")
public class TableController {

    @Autowired
    private EventRepository eventRepository;

    @Autowired
    private OrarService orarService;

    private List<Orar> getOrars(int day, int month, int year, String name, List<Orar> orarEvents) {
        if (name != null) {
            Orar orar = new Orar();
            orar.setAgileEventName(name);
            System.out.println(orar.getAgileEventName());
            orarEvents.add(orar);
            return orarEvents;
        } else {
            List<Event> events = eventRepository.findEventsByData(year, month, day);
            orarService.addEvent(orarEvents, events);

            return orarEvents;
        }
    }

    @RequestMapping(value = "eventsList1", method = RequestMethod.GET)
    @ResponseBody
    public List<Event> getEvents() {
        return eventRepository.findAll();

    }

    @RequestMapping(value = "eventsList", method = RequestMethod.GET)
    @ResponseBody
    public List<Orar> getOrarEvents() {

        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int month = calendar.get(Calendar.MONTH) + 1;
        int year = calendar.get(Calendar.YEAR);
        String name = eventRepository.findHoliday(year, month, day);
        List<Orar> orarEvents = new ArrayList<>();

        return getOrars(day, month, year, name, orarEvents);
    }


    @RequestMapping(value = "eventsList/{dateCalendar}", method = RequestMethod.GET)
    @ResponseBody
    public List<Orar> getEventsByDate(@PathVariable(value = "dateCalendar") String dateCalendar) {

        String str[] = dateCalendar.split("-");
        int day = Integer.parseInt(str[2]);
        int month = Integer.parseInt(str[1]);
        int year = Integer.parseInt(str[0]);

        String name = eventRepository.findHoliday(year, month, day);
        List<Orar> orarEvents = new ArrayList<>();

        //disable calendar weekends

        String[] weekend = dateCalendar.split("-");
        ArrayList<Integer> data = new ArrayList<>();
        for (String st : weekend)
            data.add(Integer.parseInt(st));

        Calendar calendar = Calendar.getInstance();

        calendar.set(Calendar.YEAR, data.get(0));
        calendar.set(Calendar.MONTH, data.get(1) - 1);
        calendar.set(Calendar.DAY_OF_MONTH, data.get(2));

        int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
        if (dayOfWeek == Calendar.SATURDAY || dayOfWeek == Calendar.SUNDAY) {
            return orarEvents;

        }
        return getOrars(day, month, year, name, orarEvents);
    }

    //    @DeleteMapping("/eventsList/{id}")
    @RequestMapping(value = "eventsList2/{id}", method = {RequestMethod.DELETE, RequestMethod.GET})
    @ResponseBody
    public boolean deleteEvent(@PathVariable(value = "id") Long id) {
        Event event = eventRepository.findEventById(id);
        if (event == null) {
            System.out.println("Unable to delete");
            return false;
        }
        System.out.println(event);
        eventRepository.deleteById(id);
        return true;

    }


}

