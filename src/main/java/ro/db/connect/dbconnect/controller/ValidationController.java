package ro.db.connect.dbconnect.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ro.db.connect.dbconnect.domain.Event;
import ro.db.connect.dbconnect.orarEvenimente.Orar;
import ro.db.connect.dbconnect.orarEvenimente.OrarService;
import ro.db.connect.dbconnect.repository.EventRepository;
import ro.db.connect.dbconnect.security.Code;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins="http://localhost:4200", allowedHeaders="*")
public class ValidationController {

    @Autowired
    private EventRepository eventRepository;
    @Autowired
    private OrarService orarService;

    @RequestMapping(value = "eventsListValidate", method = RequestMethod.GET)
    @ResponseBody
    public List<Orar> eventByHash(String hash) {

        List<Orar> listOrar = new ArrayList<>();

        if (hash == null || hash.isEmpty()) {
            return listOrar;
        }
        Event evNull = Code.decodeEvent(hash);

//        for (Event ev : eventRepository.findAll()) {
//            if (Code.generateSecurePassword(new String(String.valueOf(ev.getId()) + ev.getName())).equals(hash)) {
//                evNull = ev;
//                break;
//            }
//        }

        if (evNull != null) {
            Calendar calendar = evNull.getData();
            Integer year = calendar.get(Calendar.YEAR);
            Integer month = calendar.get(Calendar.MONTH);
            Integer day = calendar.get(Calendar.DAY_OF_MONTH);

            List<Event> listEvent =  eventRepository.findEventsByData(year,month,day);
            orarService.addEvent(listOrar, listEvent);

            int startOrar = 0;
            for (Orar orar : listOrar) {
                if (evNull.getOra_start() == orar.getHour()) {
                    break;
                }
                startOrar++;
            }
            int diff = evNull.getOra_end() - evNull.getOra_start() + startOrar;
            for (int i = startOrar; i < diff; i++) {
                if (evNull.getWaterfall() && !evNull.getAgile()) {
                    listOrar.get(i).setDeleteAgile(false);
                    listOrar.get(i).setDeleteWaterfall(true);
                } else if (!evNull.getWaterfall() && evNull.getAgile()) {
                    listOrar.get(i).setDeleteAgile(true);
                    listOrar.get(i).setDeleteWaterfall(false);
                } else if (evNull.getWaterfall() && evNull.getAgile()) {
                    listOrar.get(i).setDeleteAgile(true);
                    listOrar.get(i).setDeleteWaterfall(true);
                }
            }

        }
        else {
            return new ArrayList<>();
        }

        return listOrar;
    }

    @RequestMapping(value = "eventsListValidate/{code}", method = RequestMethod.GET)
    @ResponseBody
    public List<Orar> getEventsByCode(@PathVariable(value = "code") String code) {
        return eventByHash(code);
    }
}
