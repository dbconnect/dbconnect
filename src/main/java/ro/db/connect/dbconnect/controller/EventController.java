package ro.db.connect.dbconnect.controller;

import org.springframework.stereotype.Repository;
import ro.db.connect.dbconnect.domain.Event;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public class EventController {

    @PersistenceContext
    private EntityManager entityManager;


    public List<Event> findAll(Integer year, Integer month, Integer day){
        return entityManager.createNativeQuery("SELECT * FROM event WHERE year(data) = ?1 AND MONTH(data) = ?2 AND DAY(data) = ?3")
                .setParameter(1,year)
                .setParameter(2,month)
                .setParameter(3,day)
                .getResultList();
        //return createNamedQuery.getResultList();
    }
}
