package ro.db.connect.dbconnect.orarEvenimente;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ro.db.connect.dbconnect.domain.Event;
import ro.db.connect.dbconnect.repository.EventRepository;
import ro.db.connect.dbconnect.orarEvenimente.Orar;

import java.util.ArrayList;
import java.util.List;

@Repository
public class OrarService {

    public void addEvent(List<Orar> orarEvents, List<Event> events) {

        for (int i = 9; i <= 20; i++) {
            Orar orar = new Orar();
            orar.setHour(i);
            orar.setDeleteAgile(false);
            orar.setDeleteWaterfall(false);
            for (Event ev : events) {
                if (ev.getOra_start() <= i && ev.getOra_end() > i) {
                    if (ev.getAgile()) {
                        orar.setAgileEventId(ev.getId());
                        System.out.println("iata id-ul de la ev agile" + orar.getAgileEventId());
                        orar.setAgile(true);
                        orar.setAgileEventName(ev.getName());
                    }
                    if (ev.getWaterfall()) {
                        orar.setWateEventId(ev.getId());
                        System.out.println("iata id-ul de la ev  wat" + orar.getWateEventId());
                        orar.setWaterfall(true);
                        orar.setWaterfallEventName(ev.getName());
                    }
                    if( orar.getAgileEventId() == orar.getWateEventId() )
                        orar.setWaterfallEventName(null);
                }
            }
            orarEvents.add(orar);
        }
        for(int i=0, j = 1; i < orarEvents.size()-1 && j < orarEvents.size(); i++,j++){
            if( ((orarEvents.get(i)).getWateEventId()) == ((orarEvents.get(j)).getWateEventId() ))
                (orarEvents.get(j)).setWaterfallEventName(null);

            if( ((orarEvents.get(i)).getAgileEventId()) == ((orarEvents.get(j)).getAgileEventId() ))
                (orarEvents.get(j)).setAgileEventName(null);


        }

    }
}
