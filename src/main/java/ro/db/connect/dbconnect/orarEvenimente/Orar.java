package ro.db.connect.dbconnect.orarEvenimente;



public class Orar {

    private Integer hour;
    private Boolean agile=false;
    private Long agileEventId;
    private Boolean waterfall=false;
    private Long wateEventId;
    private String agileEventName;
    private String waterfallEventName;
    private Boolean deleteAgile=false;
    private Boolean deleteWaterfall=false;

    public Orar(Integer hour, Boolean agile, Long agileEventId, Boolean waterfall, Long wateEventId, String agileEventName, String waterfallEventName, Boolean deleteAgile, Boolean deleteWaterfall) {
        this.hour = hour;
        this.agile = agile;
        this.agileEventId = agileEventId;
        this.waterfall = waterfall;
        this.wateEventId = wateEventId;
        this.agileEventName = agileEventName;
        this.waterfallEventName = waterfallEventName;
        this.deleteAgile = deleteAgile;
        this.deleteWaterfall = deleteWaterfall;
    }

    public Orar() {
    }

    public Boolean getDeleteAgile() {
        return deleteAgile;
    }

    public void setDeleteAgile(Boolean deleteAgile) {
        this.deleteAgile = deleteAgile;
    }

    public Boolean getDeleteWaterfall() {
        return deleteWaterfall;
    }

    public void setDeleteWaterfall(Boolean deleteWaterfall) {
        this.deleteWaterfall = deleteWaterfall;
    }

    public Long getAgileEventId() {
        return agileEventId;
    }

    public void setAgileEventId(Long agileEventId) {
        this.agileEventId = agileEventId;
    }

    public Long getWateEventId() {
        return wateEventId;
    }

    public void setWateEventId(Long watEventId) {
        this.wateEventId = watEventId;
    }

    public Integer getHour() {
        return hour;
    }

    public void setHour(Integer hour) {
        this.hour = hour;
    }

    public Boolean getAgile() {
        return agile;
    }

    public void setAgile(Boolean agile) {
        this.agile = agile;
    }

    public Boolean getWaterfall() {
        return waterfall;
    }

    public void setWaterfall(Boolean waterfall) {
        this.waterfall = waterfall;
    }

    public String getAgileEventName() {
        return agileEventName;
    }

    public void setAgileEventName(String agileEventName) {
        this.agileEventName = agileEventName;
    }

    public String getWaterfallEventName() {
        return waterfallEventName;
    }

    public void setWaterfallEventName(String waterfallEventName) {
        this.waterfallEventName = waterfallEventName;
    }

    @Override
    public String toString() {
        return "Orar{" +
                ", hour=" + hour +
                ", agile=" + agile +
                ",agileEvId=" + agileEventId +
                ", waterfall=" + waterfall +
                ",watEvId=" + wateEventId +
                ", agileEventName='" + agileEventName + '\'' +
                ", waterfallEventName='" + waterfallEventName + '\'' +
                '}';
    }
}
