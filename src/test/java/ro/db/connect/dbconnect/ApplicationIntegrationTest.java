package ro.db.connect.dbconnect;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import ro.db.connect.dbconnect.mail.IMAPIdleConfiguration;
import static org.assertj.core.api.Assertions.assertThat;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = DbConnectApplication.class)
public class ApplicationIntegrationTest {

    @Autowired
    private IMAPIdleConfiguration imapIdleConfiguration;

    @Test
    public void contextLoads() {
        assertThat(imapIdleConfiguration).isNotNull();
    }
}