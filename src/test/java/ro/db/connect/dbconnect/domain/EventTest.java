package ro.db.connect.dbconnect.domain;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Calendar;

public class EventTest {

    Event event = new Event();

    @Before
    public void setUp(){

        event.setId((long) 1);
        event.setName("TestEvent");
        event.setNumberOfParticipants(25);
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, 2018);
        calendar.set(Calendar.MONTH, 5);
        calendar.set(Calendar.DAY_OF_MONTH, 24);
        event.setData(calendar);
        event.setOra_start(12);
        event.setOra_end(15);
        event.setWaterfall(true);
        event.setAgile(false);
    }

    @Test
    public void testGetAgile() {
        Assert.assertEquals(false, event.getAgile());
    }

    @Test
    public void testSetAgile() {
        event.setAgile(true);
        Assert.assertEquals(true, event.getAgile());
    }

    @Test
    public void testGetWaterfall() {
        Assert.assertEquals(true, event.getWaterfall());

    }

    @Test
    public void testSetWaterfall() {
        event.setWaterfall(false);
        Assert.assertEquals(false, event.getWaterfall());
    }

    @Test
    public void testGetOra_start() {
        Assert.assertEquals((Integer) 12, event.getOra_start());

    }

    @Test
    public void testSetOra_start() {

        event.setOra_start(14);
        Assert.assertEquals((Integer) 14, event.getOra_start());
    }

    @Test
    public void testGetOra_end() {
        Assert.assertEquals((Integer) 15, event.getOra_end());
    }

    @Test
    public void testSetOra_end() {
        event.setOra_end(18);
        Assert.assertEquals((Integer) 18, event.getOra_end());
    }

    @Test
    public void testGetData() {

        Assert.assertEquals(2018, event.getData().get(Calendar.YEAR));
        Assert.assertEquals(5, event.getData().get(Calendar.MONTH));
        Assert.assertEquals(24, event.getData().get(Calendar.DAY_OF_MONTH));

    }

    @Test
    public void testSetData() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, 2018);
        calendar.set(Calendar.MONTH, 6);
        calendar.set(Calendar.DAY_OF_MONTH, 12);
        event.setData(calendar);
        Assert.assertEquals(2018, event.getData().get(Calendar.YEAR));
        Assert.assertEquals(6, event.getData().get(Calendar.MONTH));
        Assert.assertEquals(12, event.getData().get(Calendar.DAY_OF_MONTH));
    }

    @Test
    public void testGetId() {
        Assert.assertEquals((Long) ((long) 1), event.getId());
    }

    @Test
    public void testSetId() {
        event.setId((long) 2);
        Assert.assertEquals((Long) ((long) 2), event.getId());
    }

    @Test
    public void testGetName() {
        Assert.assertEquals("TestEvent", event.getName());

    }

    @Test
    public void testSetName() {
        event.setName("TestSetEvent");
        Assert.assertEquals("TestSetEvent", event.getName());
    }

    @Test
    public void testGetNumberOfParticipants() {
        Assert.assertEquals((Integer) 25, event.getNumberOfParticipants());
    }

    @Test
    public void testSetNumberOfParticipants() {
        event.setNumberOfParticipants(45);
        Assert.assertEquals((Integer) 45, event.getNumberOfParticipants());
    }
}