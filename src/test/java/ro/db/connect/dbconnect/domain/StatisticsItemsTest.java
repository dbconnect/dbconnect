package ro.db.connect.dbconnect.domain;

import org.junit.Assert;
import org.junit.Test;
import ro.db.connect.dbconnect.statistics.StatisticsItem;

public class StatisticsItemsTest {

    @Test
    public void createValueWithConstructor() {
        StatisticsItem statisticsItem = new StatisticsItem("Jan", 10);

        Assert.assertEquals("Jan", statisticsItem.getLabel());
        Assert.assertEquals(Integer.valueOf(10), statisticsItem.getValue());
    }

    @Test
    public void setLabel() {
        StatisticsItem statisticsItem = new StatisticsItem();

        statisticsItem.setLabel("Feb");
        Assert.assertEquals("Feb", statisticsItem.getLabel());

        statisticsItem.setValue(20);
        Assert.assertEquals(Integer.valueOf(20), statisticsItem.getValue());
    }

    @Test(expected = IllegalArgumentException.class)
    public void errorSetLabel() {
        StatisticsItem statisticsItem = new StatisticsItem();

        statisticsItem.setLabel(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void errorSetValue() {
        StatisticsItem statisticsItem = new StatisticsItem();

        statisticsItem.setValue(null);
    }
}
