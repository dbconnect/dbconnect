package ro.db.connect.dbconnect.domain;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class UserTest {

    User user = new User();

    @Before
    public void setUp() {

        user.setCod((long) 1);
        user.setMail("dbconnectreservation@gmail.com");

    }

    @Test
    public void getCod() {
        Assert.assertEquals((Long) ((long) 1), user.getCod());
    }

    @Test
    public void setCod() {
        user.setCod((long) 2);
        Assert.assertEquals((Long) ((long) 2), user.getCod());
    }

    @Test
    public void getMail() {
        Assert.assertEquals("dbconnectreservation@gmail.com", user.getMail());
    }

    @Test
    public void setMail() {
        user.setMail("dbconnectreservation@yahoo.com");
        Assert.assertEquals("dbconnectreservation@yahoo.com", user.getMail());
    }

}