package ro.db.connect.dbconnect.domain;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Calendar;

public class HolidayTest {

    Holiday holiday = new Holiday();

    @Before
    public void setUp() {

        holiday.setId(1);
        holiday.setName("ZiuaMuncii");
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, 2018);
        calendar.set(Calendar.MONTH, 5);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        holiday.setDate(calendar);
    }

    @Test
    public void getId() {
        Assert.assertEquals((Integer) 1, holiday.getId());
    }

    @Test
    public void setId() {
        holiday.setId(3);
        Assert.assertEquals((Integer) 3, holiday.getId());

    }

    @Test
    public void getDate() {

        Assert.assertEquals(2018, holiday.getDate().get(Calendar.YEAR));
        Assert.assertEquals(5, holiday.getDate().get(Calendar.MONTH));
        Assert.assertEquals(1, holiday.getDate().get(Calendar.DAY_OF_MONTH));
    }

    @Test
    public void setDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, 2018);
        calendar.set(Calendar.MONTH, 5);
        calendar.set(Calendar.DAY_OF_MONTH, 7);
        holiday.setDate(calendar);
        Assert.assertEquals(2018, holiday.getDate().get(Calendar.YEAR));
        Assert.assertEquals(5, holiday.getDate().get(Calendar.MONTH));
        Assert.assertEquals(7, holiday.getDate().get(Calendar.DAY_OF_MONTH));
    }

    @Test
    public void getName() {
        Assert.assertEquals("ZiuaMuncii", holiday.getName());
    }

    @Test
    public void setName() {
        holiday.setName("Eroare");
        Assert.assertEquals("Eroare", holiday.getName());
    }
}

