package ro.db.connect.dbconnect.domain;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ro.db.connect.dbconnect.controller.StatisticsController;
import ro.db.connect.dbconnect.repository.EventRepository;
import ro.db.connect.dbconnect.statistics.StatisticsItem;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class StatisticsTest {

    @Autowired
    private EventRepository eventRepository;

    private List<Event> events;

    @Autowired
    private StatisticsController statisticsController;

    @Before
    public void setUp() {
    }

    @Test(expected = NullPointerException.class)
    public void verifyDataFromDB() {

        events = eventRepository.findAll();

        Assert.assertEquals(events, eventRepository.findAll());
    }

    @Test(expected = NullPointerException.class)
    public void getDayValues() {
        List<StatisticsItem> statisticsItems = statisticsController.getTheMostPopularHourByMonthByRoom(true, false);

        List<String> months = Arrays.asList("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");

        List<String> actual = new ArrayList<>();
        for (StatisticsItem stat : statisticsItems) {
            actual.add(stat.getLabel());
        }

        Assert.assertEquals(months, actual);
    }

    @Test(expected = NullPointerException.class)
    public void getValuesFromMethods() {
        List<StatisticsItem> expected = statisticsController.getTheMostPopularHourByMonthByRoom(true, false);
        List<StatisticsItem> actual = statisticsController.getMostPopularHoursByDaysOfWeekAgile();
        Assert.assertEquals(expected, actual);

        expected = statisticsController.getTheMostPopularHourByMonthByRoom(true, true);
        actual = statisticsController.getMostPopularHoursByDaysOfWeekAgileAndWaterfall();
        Assert.assertEquals(expected, actual);

        expected = statisticsController.getTheMostPopularHourByMonthByRoom(false, true);
        actual = statisticsController.getMostPopularHoursByDaysOfWeekWaterfall();
        Assert.assertEquals(expected, actual);

        expected = statisticsController.funcStat3(false, true);
        actual = statisticsController.getLeastBusyHoursByDaysWaterfall();
        Assert.assertEquals(expected, actual);

        expected = statisticsController.funcStat3(true, false);
        actual = statisticsController.getLeastBusyHoursByDaysAgile();
        Assert.assertEquals(expected, actual);

        expected = statisticsController.funcStat3(true, true);
        actual = statisticsController.getLeastBusyHoursByDaysAgileAndWaterfall();
        Assert.assertEquals(expected, actual);

        expected = statisticsController.funcStat4(true, true);
        actual = statisticsController.getMostEventsPerPeriodOfDayAgileAndWaterfall();
        Assert.assertEquals(expected, actual);

        expected = statisticsController.funcStat3(true, false);
        actual = statisticsController.getLeastBusyHoursByDaysWaterfall();
        Assert.assertEquals(expected, actual);

        expected = statisticsController.funcStat3(true, false);
        actual = statisticsController.getLeastBusyHoursByDaysWaterfall();
        Assert.assertEquals(expected, actual);

        expected = statisticsController.getMostPopularHoursByDaysOfWeek(true, true);
        actual = statisticsController.getMostPopularHoursByDaysOfWeekAgileAndWaterfall();
        Assert.assertEquals(expected, actual);

        expected = statisticsController.getMostPopularHoursByDaysOfWeek(true, false);
        actual = statisticsController.getMostPopularHoursByDaysOfWeekAgile();
        Assert.assertEquals(expected, actual);

        expected = statisticsController.getMostPopularHoursByDaysOfWeek(false, true);
        actual = statisticsController.getMostPopularHoursByDaysOfWeekWaterfall();
        Assert.assertEquals(expected, actual);

    }
}
