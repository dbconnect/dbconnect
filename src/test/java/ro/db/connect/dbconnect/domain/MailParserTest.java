package ro.db.connect.dbconnect.domain;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.StringTokenizer;


public class MailParserTest {

    public static Event isAvailable(String mail, ArrayList<Event> evenimenteDupaZi) {

        Event event = new Event();
        StringTokenizer st = new StringTokenizer(mail, " ");
        ArrayList<String> tk = new ArrayList<>();

        while (st.hasMoreTokens()) {
            tk.add(st.nextToken());
        }

        StringTokenizer stringTokenizerDate = new StringTokenizer(tk.get(2), "/");
        ArrayList<Integer> data = new ArrayList<>();
        while (stringTokenizerDate.hasMoreTokens()) {
            data.add(Integer.parseInt(stringTokenizerDate.nextToken()));
        }
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, data.get(2));

        // We will have to increment the month field by 1

        calendar.set(Calendar.MONTH, data.get(1) - 1);

        // As the month indexing starts with 0

        calendar.set(Calendar.DAY_OF_MONTH, data.get(0));

        int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
        if (dayOfWeek == Calendar.SATURDAY || dayOfWeek == Calendar.SUNDAY) {
            event.setName("Eroare:Este_weekend!");
            return event;
        } else {
            //Caut sarbatoare in ziua din mail
            //List<Holiday> sarbatoare = holidayRepository.findHolidays(data.get(2), data.get(1), data.get(0));

            if (0 == 0) {


                //Iau evenimentele din ziua data in email


                Boolean[][] tabelOcupare = new Boolean[2][11];

                Boolean eventAgile = Boolean.FALSE,
                        eventWaterfall = Boolean.FALSE,
                        eventValidat;

                //Ziua este goala si se poate pune evenimentul la ora dorita
                if (evenimenteDupaZi.size() == 0) {
                    if (Integer.parseInt(tk.get(1)) < 50) {
                        eventAgile = Boolean.FALSE;
                        eventWaterfall = Boolean.TRUE;
                    } else {
                        eventAgile = Boolean.TRUE;
                        eventWaterfall = Boolean.TRUE;
                    }

                    event.setName(tk.get(0));

                    if (eventAgile == Boolean.FALSE) {
                        event.setAgile(false);
                    } else {
                        event.setAgile(true);
                    }

                    event.setWaterfall(true);

                    //Ora de start si de end din email
                    StringTokenizer stInt = new StringTokenizer(tk.get(3), "-");
                    Integer start = Integer.parseInt(stInt.nextToken());
                    Integer end = Integer.parseInt(stInt.nextToken());

                    event.setOra_start(start);
                    event.setOra_end(end);
                    event.setData(calendar);
                    return event;
                }

                for (int i = 0; i < 11; i++) {
                    tabelOcupare[0][i] = Boolean.FALSE;
                    tabelOcupare[1][i] = Boolean.FALSE;
                }
                //completare matrice cu orele corespunzatoare
                for (Event ev : evenimenteDupaZi) {
                    //int durata = ev.getOra_end() - ev.getOra_start();
                    int s = ev.getOra_start() - 8;
                    int e = ev.getOra_end() - 8;
                    for (int i = s; i < e; i++) {
                        if (ev.getWaterfall()) {
                            tabelOcupare[0][i] = Boolean.TRUE;
                        }
                        if (ev.getAgile()) {
                            tabelOcupare[1][i] = Boolean.TRUE;
                        }
                    }
                }

                eventWaterfall = Boolean.TRUE;// AICI ERAU FALSE
                eventAgile = Boolean.TRUE;

                //Ora de start si de end din email
                StringTokenizer stInt = new StringTokenizer(tk.get(3), "-");
                Integer start = Integer.parseInt(stInt.nextToken());
                Integer end = Integer.parseInt(stInt.nextToken());

                for (int i = start; i < end; i++) {
                    if ((!tabelOcupare[0][i - 8]) && (eventWaterfall == Boolean.TRUE)) {
                        eventWaterfall = Boolean.TRUE;
                    } else {
                        eventWaterfall = Boolean.FALSE;
                    }

                    if ((!tabelOcupare[1][i - 8]) && (eventAgile == Boolean.TRUE)) {
                        eventAgile = Boolean.TRUE;
                    } else {
                        eventAgile = Boolean.FALSE;
                    }
                }

                if (Integer.parseInt(tk.get(1)) < 50) {
                    if (eventWaterfall == Boolean.TRUE) {
                        eventAgile = Boolean.FALSE;
                        eventValidat = Boolean.TRUE;
                    } else if (eventAgile == Boolean.TRUE) {
                        eventValidat = Boolean.TRUE;
                    } else {
                        eventValidat = Boolean.FALSE;
                    }
                } else {
                    if (eventWaterfall == Boolean.TRUE && eventAgile == Boolean.TRUE) {
                        eventValidat = Boolean.TRUE;
                    } else eventValidat = Boolean.FALSE;
                }

                if (eventValidat == Boolean.TRUE) {
                    //Setam numele
                    event.setName(tk.get(0));
                    //Setam salile
                    if (eventAgile != Boolean.TRUE) {
                        event.setAgile(false);
                    } else {
                        event.setAgile(true);
                    }
                    if (eventWaterfall != Boolean.TRUE) {
                        event.setWaterfall(false);
                    } else {
                        event.setWaterfall(true);
                    }
                    //Setam ora si data
                    event.setOra_start(start);
                    event.setOra_end(end);
                    //  event.setData(calendar);
                    //Salvam event-ul in baza de date
                    return event;
                }
            } else {
                event.setName("Eroare:Este_Sarbatoare!");
                return event;
            }
        }
        return null;
    }
}
