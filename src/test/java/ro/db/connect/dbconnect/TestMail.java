package ro.db.connect.dbconnect;


import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ro.db.connect.dbconnect.domain.Event;
import ro.db.connect.dbconnect.domain.MailParserTest;
import ro.db.connect.dbconnect.mail.MailParser;

import java.util.ArrayList;
import java.util.Calendar;


public class TestMail {
    Event event;
    private String mail;
    private MailParserTest mailParserTest;

    @Before
    public void setUp()
    {
        mail = "java 20 17/04/2018 10-12";

    }

    @Test
    public void testParser()
    {
        Assert.assertEquals(true, MailParser.isValid(mail));
        mail = "java 145 17/04/2018 10-14";
        Assert.assertEquals(false, MailParser.isValid(mail));
        mail = "";
        Assert.assertEquals(false, MailParser.isValid(mail));
        mail = "ceva -2 17/04/2018 12-13";
        Assert.assertEquals(false, MailParser.isValid(mail));
        mail = "altceva 10 18/04/2018 20-22";
        Assert.assertEquals(false, MailParser.isValid(mail));
        mail = "java 20 14/04/2018 8-10";
        Assert.assertEquals(false, MailParser.isValid(mail));
        mail = "java 20 14/12/2018 8-10";
        Assert.assertEquals(false, MailParser.isValid(mail));
        mail = "java 20 14/12.2018 8-10";
        Assert.assertEquals(false, MailParser.isValid(mail));
        mail = "java 20 1a/12/2018 8-10";
        Assert.assertEquals(false, MailParser.isValid(mail));
        mail = "java 20 14/12.2/2018 8-10";
        Assert.assertEquals(false, MailParser.isValid(mail));
        mail = "java 2a 1/5/2018 8-10";
        Assert.assertEquals(false, MailParser.isValid(mail));
        mail = "java 20 14/12.2/2018 8a-10";
        Assert.assertEquals(false, MailParser.isValid(mail));
        mail = "java 20 14/12/2018 8-10a";
        Assert.assertEquals(false, MailParser.isValid(mail));
        mail = "java 20-30 14/12/2018 8-10";
        Assert.assertEquals(false, MailParser.isValid(mail));
        mail = "java -30 14/12/2018 8-10";
        Assert.assertEquals(false, MailParser.isValid(mail));
        mail = "java 20-30 14-8/12/2018 8-10";
        Assert.assertEquals(false, MailParser.isValid(mail));
        mail = "java 20-30 14/12/2 8-10";
        Assert.assertEquals(false, MailParser.isValid(mail));
    }


    @Test
    public void testIsAvailable() {

        ArrayList<Event> evenimente = new ArrayList<>();


        Event ev = new Event();
        ev.setName("BEX");
        Calendar cl = Calendar.getInstance();
        cl.set(Calendar.YEAR, 2018);
        cl.set(Calendar.MONTH, 5);
        cl.set(Calendar.DAY_OF_MONTH, 24);
        ev.setData(cl);
        ev.setOra_start(10);
        ev.setOra_end(13);
        ev.setNumberOfParticipants(23);
        ev.setWaterfall(true);
        ev.setAgile(true);
        evenimente.add(ev);

        Event ev1 = new Event();
        ev1.setName("AAA");
        Calendar cl1 = Calendar.getInstance();
        cl1.set(Calendar.YEAR, 2018);
        cl1.set(Calendar.MONTH, 5);
        cl1.set(Calendar.DAY_OF_MONTH, 24);
        ev1.setData(cl1);
        ev1.setOra_start(11);
        ev1.setOra_end(12);
        ev1.setNumberOfParticipants(23);
        ev1.setWaterfall(true);
        ev1.setAgile(false);
        //    evenimente.add(ev1);

        Event event = new Event();
        event.setName("III");
        event.setOra_start(11);
        event.setOra_end(13);
        event.setWaterfall(false);
        event.setAgile(true);


        mail = "III 12 24/5/2018 11-12";


        Event found = MailParserTest.isAvailable(mail, evenimente);

        Assert.assertEquals(null, found);

    }
}
