#Tutorial pentru a face pull request:

##Inainte de toate :Pull pe master sa aveti ultima versiune


**1.** Creati un branch in sourcetree si faceti checkout pe el (va mutati pe el)

**2.** Lucrati la ce task vreti sa implementati 

**3.** Cand aveti task-ul functional : faceti commit si push pe branch (daca mai lucreaza cineva pe branch=ul respectiv cu voi, faceti si pull dupa commit)

**4.** Va duceti pe bitbucket -> pull requests -> create pull request -> selectati branch-ul vostru in stanga si master in dreapta 

**5.** Dupa ce se face merge cu branch-ul , puteti sa il stergeti

**6.** La final faceti iar pull pe master pentru a avea si branch-ul merge-uit

#Code formatting automat la salvare:

**1.** Mergeti si descarcati plugin-ul : https://plugins.jetbrains.com/plugin/7642-save-actions

**2.** Il puneti in folderul 'plugins' din IDEA ( acolo unde este instalat)

**3.** In IDEA : File-> Settings -> Plugins -> Install plugin from disk -> selectati din folder intellij-plugin-save

**4.** Restartati intellij


**5.** Dupa restart o sa va apara la settings Save Actions

**6.** Faceti setarile

![Scheme](save.png)
